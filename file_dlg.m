function [f, p] = file_dlg(ui_dlg_fn, varargin)
% uiget/putfile doesn't "remember" last folder and returns different data types
% for f depending on if it's a single or multiple files that has been selected,
% hence this wrapper
	persistent sp;
	cdtosp = (ischar(sp) && isfolder(sp));
	if (cdtosp)
		currdir = pwd();
		cd(sp);
	end
	[f, p] = ui_dlg_fn(varargin{:});
	if (cdtosp); cd(currdir); end
	if (isa(f, 'char')); f = {f}; end
	if (isa(p, 'char')); sp = p; end
end
