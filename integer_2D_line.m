function pts = integer_2D_line(varargin)
	p0 = [0 0];
	if nargin == 1
		p1 = varargin{1};
	elseif nargin == 2
		p0 = varargin{1};
		p1 = varargin{2};
	else
		error("Argument error! Must be (p1), or (p0, p1).");
	end
	assert(all(size(p0) == [1 2]) && all(size(p0) == size(p1)), ...
		   "Wrong size input");
	assert(~all(round(p0) == round(p1)), "Rounded, the points are equal");
	if round(p0(1)) == round(p1(1))
		y = (p0(2) : sign(p1(2)-p0(2)) : p1(2))';
		pts = [round(p0(1)) .* ones(size(y)), y];
		return;
	end
	a = (p1(2)-p0(2)) / (p1(1)-p0(1));
	b = p0(2) - a*p0(1);
	x = (p0(1) : sign(p1(1)-p0(1)) : p1(1))';
	y = @(x) round(a.*x + b);
	pts = [x, y(x)];
end
