function [x_span, interp_y] = interp_xy(x, y, varargin)
% By default interp_xy locates the biggest overlapping region in x's columns and
% generates n_pts in it, i.e. x_span = max(min(x)) : dx : min(max(x)), where
% dx = (min(max(x)) - max(min(x))) / (n_pts - 1), and then linearly interpolates
% corresponding values for the y columns (default value for n_pts = 20). Note
% that this function uses matlab's stable sort, meaning that if there are two
% values in a y column corresponding to the same x-value the first will be used
% when interpolating - remember this if y(x) describes a system with hysteresis.
%
% Use case:
% When you have multiple sets of measurements of x and y sampled independetly of
% each other and you are looking for an y(x) relation.
% 
% Arguments:
% x	- 2D matrix with observations in the coulmns
% y	- 2D matrix of the same size as x with corresponding y values
%
% Possible name-value pairs:
% 'x_span'	- if given its value is used instead of calculated
% 'n_pts'	- integer number of points in x_span (ignored if x_span is set),
%			  default: 20
% 's_frac'	- if value is a scalar then it indicates the (centered) fraction of
%			  the calculated x_span to use, if it's a two column vector then the
%			  first element indicates the fraction of x_span to start at and the
%			  second the fraction to end at (ignored if x_span is set),
%			  default: 1 (equivalent to [0 1])
%
% Returns:
% [x_span, interp_y] = interp_xy(x, y):
% x_span	- n_pts long vector of biggest overlapping region in x-columns (or
%			  the given 'x_span' value)
% interp_y	- interpolated y-values
%
% Dependencies:
% * parse_name_value_pairs.m
	[n_pts, x_span, s_frac] = parse_name_value_pairs(varargin, ...
													 'n_pts', 20, ...
													 'x_span', [], ...
													 's_frac', 1);
	mmax = min(max(x));
	mmin = max(min(x));
	if (isempty(x_span))
		mmsz = mmax - mmin;
		if (numel(s_frac) == 1)
			assert(0 < s_frac && s_frac <= 1, ...
				   sprintf('Invalid s_frac value (%g)', s_frac));
			s = mmsz * (1 - s_frac) / 2;
			mmin = mmin + s;
			mmax = mmax - s;
		elseif (numel(s_frac) == 2)
			assert(all(0 <= s_frac & s_frac <= 1) && s_frac(1) < s_frac(2), ...
				   'Invalid s_frac pair ([%g, %g])', s_frac);
			mmin = mmin + mmsz * s_frac(1);
			mmax = mmax - mmsz * (1 - s_frac(2));
		else
			error('Invalid number of s_frac elements (%g)', numel(s_frac));
		end
		dx = (mmax - mmin) / (n_pts - 1);
		% sanity checks
		assert(dx > 0, 'There is no overlapping region in all of x''s columns');
		if ((mmax - mmin) / (max(x, [], 'all') - min(x, [], 'all')) < 0.5)
			warning('The overlap in x''s columns is less than 0.5 (%.3f) of its total span', ...
					(mmax - mmin) / (max(x,[],'all') - min(x,[],'all')));
		end
		x_span = mmin : dx : mmax;
	else
		assert(mmin <= min(x_span) && max(x_span) <= mmax, ...
			   'Supplied x_span is not in an overlapping region of x');
		n_pts = numel(x_span);
	end
	% this is just to ensure consistent output
	x_span = reshape(x_span, [], 1);
	% [r, c] indexing to matrix indexing
	rc2m_idx = @(rc) (0 : (size(rc, 2)-1)) .* size(rc,1);
	[sx, sx_i] = sort(x);
	sx_i = sx_i + rc2m_idx(sx_i);

	va_i = zeros(1, size(y, 2));
	vb_i = zeros(size(va_i));
	vab_inc = rc2m_idx(sx);
	interp_y = zeros(n_pts, size(y, 2));
	for i = 1 : n_pts
		for j = 1 : length(va_i)
			va_i(j) = find(sx(:, j) >= x_span(i), 1); % 1st point above
			vb_i(j) = find(sx(:, j) <= x_span(i), 1, 'last'); % 1st below
			if (va_i(j) <= vb_i(j))
				% the weights will take care of the rest
				if (va_i(j) == 1); va_i(j) = vb_i(j) + 1;
				else; vb_i(j) = va_i(j) - 1;
				end
			end
		end
		va_i = va_i + vab_inc;
		vb_i = vb_i + vab_inc;
		d = sx(va_i) - sx(vb_i);
		a_weight = (x_span(i) - sx(vb_i)) ./ d;
		b_weight = (sx(va_i) - x_span(i)) ./ d;
		interp_y(i, :) = b_weight .* y(sx_i(vb_i)) + a_weight .* y(sx_i(va_i));
	end
end
