function varargout = export_eps(varargin)
% This is just to get around matlab's weirdness with eps-exports where there's
% (sometimes at least) way to much whitespace between superscripts and their
% base. Does it by first exporting an svg, using inkscape to convert it to eps,
% and then delete the svg again.
% Doesn't work on windows (for now).
%
% possible argument combos:
% export_eps()		- uses gcf and shows you a save-file dialog
% export_eps(f)		- uses figure f and shows you a save-file dialog
% export_eps(fn)	- uses gcf and saves to filename fn
% export_eps(f, fn)	- saves figure f to filename fn
%
% returns (same for all argument combos):
% export_eps()				- returns nothing and error()s if inkscape call
%							  doesn't return 0
% c = export_eps()			- returns inkscape's return value, doesn't error()
%							  on c ~= 0
% [c, cmd] = export_eps()	- returns inkscape's return value and the full
%							  inkscape call, doesn't error() and doesn't delete
%							  the temporary svg file on c ~= 0
%
% NOTE:
% Will create a file named 
%
% DEPENDENCIES:
% - inkscape (available in PATH as just "inkscape")
% - file_dlg (from my utils)
%
% TODO:
% - make it work with arrays of figures/filenames
% - make exportLatexFig('/all/') export all currently open figures
	assert(~ispc(), 'This doesn''t work on windows yet');
	assert(nargin < 3, 'That''s too many arguments, Bucko');
	fn = '';
	f = get(groot, 'CurrentFigure');
	% this doesn't really check
	for i = 1 : nargin
		if (isa(varargin{i}, 'matlab.ui.Figure'))
			f = varargin{i};
		elseif (isa(varargin{i}, 'char') || isa(varargin{i}, 'string'))
			fn = varargin{i};
		else
			error('export_eps:arginclass', ...
				  'Unknown argument class %s.', class(varargin{i}));
		end
	end
	assert(isa(f, 'matlab.ui.Figure'), ...
		   'No figure supplied and no current figure found');
	if (length(fn) < 3)
		[fn, p] = file_dlg(@uiputfile, ...
						   { '*.eps', 'Encapsulated PostScript (*.eps)' }, ...
						   sprintf('Save figure (%d) "%s" to file', ...
								   f.Number, f.Name));
		assert(~isa(p, 'double'), 'You chose to cancel');
		fn = [p, fn{1}];
	else
		pi = find(fn == filesep(), 1, 'last');
		if (isempty(pi)); p = '.';
		else; p = fn(1:(pi-1));
		end
	end
	fn_svg = [tempname(p), '.svg'];
	saveas(f, fn_svg, 'svg');
	% set up the environment (need to check this on macos), i know isunix() is
	% superfluous here but what if matlab is released for like.. Haiku?
	if (isunix() && ~ismac())
		ml_llp = getenv('LD_LIBRARY_PATH');
		setenv('LD_LIBRARY_PATH', getenv('PATH'));
	else
		% TODO
	end
	cmd = ['inkscape -z "', fn_svg, '" ', ...	% explicitly no-gui
		   '--export-filename="', fn, '" ', ...	% export type given by filename
		   '--export-ignore-filters ', ...		% don't rasterize filter
		   '--export-ps-level=3 ', ...			% eps level 3
		   '&> /dev/null'];						% redir away warnings
	c = system(cmd);						
	if (isunix() && ~ismac())
		setenv('LD_LIBRARY_PATH', ml_llp);
	else
		% TODO
	end
	if (c == 0 || nargout < 2); delete(fn_svg); end
	if (nargout == 0); assert(c == 0, 'inkscape call returned %d', c);
	else; varargout{1} = c;
		if (nargout > 1); varargout{2} = cmd; end
	end
end

