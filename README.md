## Description
Some MATLAB utils i use mainly at work, so they should work in the latest versions of matlab on linux, and macos, and sometimes windows. They should all contain a help section, below follows some extra info.

## add_axes.m
Essentially adds a copy of the X and Y axes on top and to the right of a plot.

## clr2matRGB.m
Converts some common color strings, e.g. #abcdef, #123, rgba(12,34,56,78), to matlab's [r g b], where 0 <= r,g,b <= 1 (also alpha if supplied), format. Got tired of manually converting colors every time i used a color picker.

## cmp_struct.m
Basically a bit more strict and verbose `isequaln` (which is used internally) that only works for two structs. For the two to be considered equal all fields must exist in both and each field must be equal in class, length, and value. So while `isequaln(struct('a', 'str'), struct('a', "str"))` returns true, `cmp_struct` would not.

## export_eps.m and file_dlg.m
Inkscape-based workaround for matlabs sometimes weird eps-exports (when using superscipt in latex causes way too much whitespace) and a uiput/getfile wrapper that "remembers" the last used folder.

## fit_triangle_wave.m
fits a triangle wave to `s(t) = offset + amplitude*abs(mod((t+dt)/period, 1)-0.5)*2` given s (and optionally t).

## integer_2D_line.m
Handy when you are exceptionally lazy and want a line of indices in a matrix.

## interp_xy.m
Weighted linear interpolation of the parts of the y-columns that correspond to the biggest overlapping region in the x-columns (basically `max(min(x)) : min(max(x))`).

## lab_utils.m
A class with small util functions to deal with different instruments, common conversions, and such.

## greek.m and schar.m
Since setting `'Interpreter','latex'` sometimes uses weird fonts, isn't always
necessary(?), only work in a few controls, and since other than the odd equation $\mu, \Delta, \delta, \Theta, \lambda, \nu, \omega, \sigma, \rho,$
and $\pm, ^{\circ}$ covers around 99% of the special chars i use i decided to smoosh
them into functions

## uiedit.m
Wrote this puppy back when i did my master's. The description i wrote back then was "The ambition of UIEDIT is to make it a self-contained drop in replacement for
`uicontrol('Style', 'edit')` with the unremoved power of the underlying java
controls. As it's far from complete there's a `getJavaProp(...)` method and a
corresponding `setJavaProp(prop, val, ...)` as well as a `javaCtrlFcn(...)`.
The reason i started writing this was that i was missing a bunch of
functionality in the standard control.. as not all of them were native to
javax.swing.JTextComponent either i ended up with the following custom
properties: `AutoCompleteFcn`, `AutoCompleteStartLength`, `NumbersOnly`,
`AutoSize`, and `UnwrappedCallbacks`."

See the source/help for more info

## placeholder_fig.m
When writing papers i sometimes start by planning out the figures. I use this to generate placeholders  that are a little more descriptive of what's gonna go there than just a white box.

## readThorlabsCSV.m
Function that reads and parses the comma separated value-files produces by
Thorlabs' OSA software.

## visual_data_editor.m
Displays an input matrix with imagesc and lets you select areas to set to NaN. I've mainly used it to clean up noisy data sets from my [Zygo viewer](https://gitlab.com/ragnarseton/view_zygo).

# License
Unlicense, do whatever the heck you want!
