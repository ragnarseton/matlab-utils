function equal = cmp_struct(s1, s2, opts)
% Basically a bit more strict and verbose isequaln (which is used internally)
% that only works for two structs. For the two to be considered equal all fields
% must exist in both and each field must be equal in class, length, and value.
% So while isequaln(struct('a', 'str'), struct('a', "str")) returns true,
% cmp_struct would not.
% 
% equal = cmp_struct(s1, s2) returns true the structs s1 and s2 are equal, false
% otherwise.
%
% equal = cmp_struct(s1, s2, Verbose = v) for 0 < v <= 1 all differences between
% the structs will be echoed and for v > 1 both equality and differences will
% be echoed. Default value is 0.
%
% equal = cmp_struct(s1, s2, LazyEval = false) will not return as soon as a
% difference is found (mainly intended to be used with Verbose > 0). Default
% value is true. 
%
% See also
% isequaln, isequal
arguments
    s1 (1,1) struct;
    s2 (1,1) struct;
    opts.Verbose (1,1) {mustBeNumeric} = 0;
    opts.LazyEval (1,1) logical = true;
end
    % check that the fields are the same, assume not
    equal = false;
    fn1 = fieldnames(s1);
    fn2 = fieldnames(s2);
    in_1_not_2 = setdiff(fn1, fn2);
    if ~isempty(in_1_not_2)
        if opts.Verbose > 0
            fprintf('Fields in s1 but not in s2: %s.\n', ...
                    strjoin(in_1_not_2, ', '));
        end
        if opts.LazyEval; return; end
    elseif opts.Verbose > 1
        fprintf('All fields of s1 are also present in s2.\n');
    end
    in_2_not_1 = setdiff(fn2, fn1);
    if ~isempty(in_2_not_1)
        if opts.Verbose > 0
            fprintf('Fields in s2 but not in s1: %s.\n', ...
                    strjoin(in_2_not_1, ', '));
        end
        if opts.LazyEval; return; end
    elseif opts.Verbose > 1
        fprintf('All fields of s2 are also present in s1.\n');
    end
    
    % check content
    equal = isempty(in_1_not_2) & isempty(in_2_not_1);
    for f = reshape(intersect(fn1, fn2), 1, [])
        if ~strcmp(class(s1.(f{1})), class(s2.(f{1})))
            equal = false;
            if opts.Verbose > 0
                fprintf('Field %s differs in class, s1: %s, s2: %s\n', ...
                        f{1}, class(s1.(f{1})), class(s2.(f{1})));
            end
            if opts.LazyEval; return;
            else; continue;
            end
        end

        ne1 = numel(s1.(f{1}));
        if ne1 ~= numel(s2.(f{1}))
            equal = false;
            if opts.Verbose > 0
                fprintf('Field %s differs in length, s1: %d, s2: %d\n', ...
                        f{1}, ne1, numel(s2.(f{1})));
            end
            if opts.LazyEval; return;
            else; continue;
            end
        end

        if ~isequaln(s1.(f{1}), s2.(f{1}))
            equal = false;
            if opts.Verbose > 0
                fprintf('Field %s differs in value.\n', f{1});
            end
            if opts.LazyEval; return; end
        elseif opts.Verbose > 1
            fprintf('Field %s is equal.\n', f{1});
        end
    end
end
