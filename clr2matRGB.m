function mv = clr2matRGB(s, varargin)
% just converts color strings that are copied from different color pickers to
% the matlab [R, G, B] format, where 0 <= R,G,B <= 1.
% acceptable formats are #RGB, #RRGGBB, #RRGGBBAA, 0xRGB, 0xRRGGBB, 0xRRGGBBAA,
% rgb(R,G,B), and rgba(R,G,B,A). In the last two whitespace between parens,
% commas and values are all ignored so, for example, rgb( 123, 321, 23 ) works
% just fine.
%
% args:
% any number of strings/1D char-arrays with valid format (see above) colors.
%
% optional args:
% dump - if set to true a copy-pasteable string of matlab code containing all
%        input colors a printed.
%
% limitation:
% - either none or all colors should have an alpha-component. i'll fix that when
%   i need it.
% - values are not validated so the script will happily output invalid colors if
%   the input values are too high.
%
% examples:
%   % save matlab color for later use
%   mrgb = clr2matRGB('#be8797');
% 
%   % output a bunch of colors
%   clr2matRGB('#345', "0x8338ec", 'rgb(12, 91, 127)', true);
%
% TODO:
% - HSL
% - named colors

    r = convert(s);
    dump = false;
    if nargin > 1
        n = nargin;
        if isa(varargin{end}, 'logical')
            dump = varargin{end};
            n = n - 1;
        end
        mv = zeros(n, length(r));
        mv(1,:) = r;
        for c = 1:(n-1); mv(c+1,:) = convert(varargin{c}); end
    else
        mv = r;
    end
    
    if dump
        pat = repmat('%.6f ', 1, size(mv,2));
        if size(mv,1) > 1
            %pat = ['\t', pat(1:end-1), '\n'];
            %fprintf('clr = [ ...\n');
            pat = [' ', pat(1:end-1), ';'];
            fprintf('clr = [ ');
            for c = 1:size(mv,1); fprintf(pat, mv(c,:)); end
            fprintf('];\n');
        else
            fprintf(['clr = [ ', pat, '];\n'], mv);
        end
    end
    
    function mv = convert(s)
        % this one can absolutely be improved...
        s = char(lower(strtrim(s)));
        mv = [0,0,0];
        ln_s = length(s);
        assert(ln_s > 3);
        if s(1) == '#' || (any(ln_s == [5, 8, 10]) && strcmp(s(1:2), '0x'))
            so = 2 + (s(1) == '0');
            hs = s(so:end);
            assert(~isempty(regexp(hs, '^[a-f\d]{3,8}$', 'once')), ...
                   'Wrong hex color format: %s', s);
            if ln_s == so-1 + 3
                for i = 1:3; mv(i) = hex2dec([hs(i), hs(i)])/255; end
            elseif ln_s == so-1 + 6 || ln_s == so-1 + 8
                for i = 1:2:(ln_s-2)
                    mv((i+1)/2) = hex2dec(hs(i+[0,1])) / 255;
                end
            else
                error('Unknown hex color format: %s', s);
            end
        elseif strcmp(s(1:3), 'rgb')
            cs = s(4:end);
            flds = 'RGB';
            if cs(1) == 'a'
                t = regexp(cs(2:end), '\s*\(\s*(?<R>\d{1,3})\s*,\s*(?<G>\d{1,3})\s*,\s*(?<B>\d{1,3})\s*,\s*(?<A>\d{1,3})\s*\)$', 'names');
                flds = 'RGBA';
            else
                t = regexp(cs, '\s*\(\s*(?<R>\d{1,3})\s*,\s*(?<G>\d{1,3})\s*,\s*(?<B>\d{1,3})\s*\)$', 'names');
            end
            assert(~isempty(t), 'Unknown RGB color format: %s', s);
            for i = 1:length(flds)
                mv(i) = str2double(t.(flds(i)))/255;
                assert(mv(i) <= 1, 'To big %c value in %s', flds(i), s);
            end
        elseif strcmp(s(1:3), 'hsl')
            % TODO
            error('Haven''t made the HSL thing yet');
        else
            error('Unknown color format: %s', s);
        end
    end
    
    function hex = name2hex(nm)
        % straight stolen from https://www.w3schools.com/lib/w3color.js
        nms = {'AliceBlue','AntiqueWhite','Aqua','Aquamarine','Azure','Beige','Bisque','Black','BlanchedAlmond','Blue','BlueViolet','Brown','BurlyWood','CadetBlue','Chartreuse','Chocolate','Coral','CornflowerBlue','Cornsilk','Crimson','Cyan','DarkBlue','DarkCyan','DarkGoldenRod','DarkGray','DarkGrey','DarkGreen','DarkKhaki','DarkMagenta','DarkOliveGreen','DarkOrange','DarkOrchid','DarkRed','DarkSalmon','DarkSeaGreen','DarkSlateBlue','DarkSlateGray','DarkSlateGrey','DarkTurquoise','DarkViolet','DeepPink','DeepSkyBlue','DimGray','DimGrey','DodgerBlue','FireBrick','FloralWhite','ForestGreen','Fuchsia','Gainsboro','GhostWhite','Gold','GoldenRod','Gray','Grey','Green','GreenYellow','HoneyDew','HotPink','IndianRed','Indigo','Ivory','Khaki','Lavender','LavenderBlush','LawnGreen','LemonChiffon','LightBlue','LightCoral','LightCyan','LightGoldenRodYellow','LightGray','LightGrey','LightGreen','LightPink','LightSalmon','LightSeaGreen','LightSkyBlue','LightSlateGray','LightSlateGrey','LightSteelBlue','LightYellow','Lime','LimeGreen','Linen','Magenta','Maroon','MediumAquaMarine','MediumBlue','MediumOrchid','MediumPurple','MediumSeaGreen','MediumSlateBlue','MediumSpringGreen','MediumTurquoise','MediumVioletRed','MidnightBlue','MintCream','MistyRose','Moccasin','NavajoWhite','Navy','OldLace','Olive','OliveDrab','Orange','OrangeRed','Orchid','PaleGoldenRod','PaleGreen','PaleTurquoise','PaleVioletRed','PapayaWhip','PeachPuff','Peru','Pink','Plum','PowderBlue','Purple','RebeccaPurple','Red','RosyBrown','RoyalBlue','SaddleBrown','Salmon','SandyBrown','SeaGreen','SeaShell','Sienna','Silver','SkyBlue','SlateBlue','SlateGray','SlateGrey','Snow','SpringGreen','SteelBlue','Tan','Teal','Thistle','Tomato','Turquoise','Violet','Wheat','White','WhiteSmoke','Yellow','YellowGreen'};
        clr = [0.941176 0.972549 1; 0.980392 0.921569 0.843137; 0 1 1; 0.498039 1 0.831373; 0.941176 1 1; 0.960784 0.960784 0.862745; 1 0.894118 0.768627; 0 0 0; 1 0.921569 0.803922; 0 0 1; 0.541176 0.168627 0.886275; 0.647059 0.164706 0.164706; 0.870588 0.721569 0.529412; 0.372549 0.619608 0.627451; 0.498039 1 0; 0.823529 0.411765 0.117647; 1 0.498039 0.313725; 0.392157 0.584314 0.929412; 1 0.972549 0.862745; 0.862745 0.078431 0.235294; 0 1 1; 0 0 0.545098; 0 0.545098 0.545098; 0.721569 0.525490 0.043137; 0.662745 0.662745 0.662745; 0.662745 0.662745 0.662745; 0 0.392157 0; 0.741176 0.717647 0.419608; 0.545098 0 0.545098; 0.333333 0.419608 0.184314; 1 0.549020 0; 0.6 0.196078 0.8; 0.545098 0 0; 0.913725 0.588235 0.478431; 0.560784 0.737255 0.560784; 0.282353 0.239216 0.545098; 0.184314 0.309804 0.309804; 0.184314 0.309804 0.309804; 0 0.807843 0.819608; 0.580392 0 0.827451; 1 0.078431 0.576471; 0 0.749020 1; 0.411765 0.411765 0.411765; 0.411765 0.411765 0.411765; 0.117647 0.564706 1; 0.698039 0.133333 0.133333; 1 0.980392 0.941176; 0.133333 0.545098 0.133333; 1 0 1; 0.862745 0.862745 0.862745; 0.972549 0.972549 1; 1 0.843137 0; 0.854902 0.647059 0.125490; 0.501961 0.501961 0.501961; 0.501961 0.501961 0.501961; 0 0.501961 0; 0.678431 1 0.184314; 0.941176 1 0.941176; 1 0.411765 0.705882; 0.803922 0.360784 0.360784; 0.294118 0 0.509804; 1 1 0.941176; 0.941176 0.901961 0.549020; 0.901961 0.901961 0.980392; 1 0.941176 0.960784; 0.486275 0.988235 0; 1 0.980392 0.803922; 0.678431 0.847059 0.901961; 0.941176 0.501961 0.501961; 0.878431 1 1; 0.980392 0.980392 0.823529; 0.827451 0.827451 0.827451; 0.827451 0.827451 0.827451; 0.564706 0.933333 0.564706; 1 0.713725 0.756863; 1 0.627451 0.478431; 0.125490 0.698039 0.666667; 0.529412 0.807843 0.980392; 0.466667 0.533333 0.6; 0.466667 0.533333 0.6; 0.690196 0.768627 0.870588; 1 1 0.878431; 0 1 0; 0.196078 0.803922 0.196078; 0.980392 0.941176 0.901961; 1 0 1; 0.501961 0 0; 0.4 0.803922 0.666667; 0 0 0.803922; 0.729412 0.333333 0.827451; 0.576471 0.439216 0.858824; 0.235294 0.701961 0.443137; 0.482353 0.407843 0.933333; 0 0.980392 0.603922; 0.282353 0.819608 0.8; 0.780392 0.082353 0.521569; 0.098039 0.098039 0.439216; 0.960784 1 0.980392; 1 0.894118 0.882353; 1 0.894118 0.709804; 1 0.870588 0.678431; 0 0 0.501961; 0.992157 0.960784 0.901961; 0.501961 0.501961 0; 0.419608 0.556863 0.137255; 1 0.647059 0; 1 0.270588 0; 0.854902 0.439216 0.839216; 0.933333 0.909804 0.666667; 0.596078 0.984314 0.596078; 0.686275 0.933333 0.933333; 0.858824 0.439216 0.576471; 1 0.937255 0.835294; 1 0.854902 0.725490; 0.803922 0.521569 0.247059; 1 0.752941 0.796078; 0.866667 0.627451 0.866667; 0.690196 0.878431 0.901961; 0.501961 0 0.501961; 0.4 0.2 0.6; 1 0 0; 0.737255 0.560784 0.560784; 0.254902 0.411765 0.882353; 0.545098 0.270588 0.074510; 0.980392 0.501961 0.447059; 0.956863 0.643137 0.376471; 0.180392 0.545098 0.341176; 1 0.960784 0.933333; 0.627451 0.321569 0.176471; 0.752941 0.752941 0.752941; 0.529412 0.807843 0.921569; 0.415686 0.352941 0.803922; 0.439216 0.501961 0.564706; 0.439216 0.501961 0.564706; 1 0.980392 0.980392; 0 1 0.498039; 0.274510 0.509804 0.705882; 0.823529 0.705882 0.549020; 0 0.501961 0.501961; 0.847059 0.749020 0.847059; 1 0.388235 0.278431; 0.250980 0.878431 0.815686; 0.933333 0.509804 0.933333; 0.960784 0.870588 0.701961; 1 1 1; 0.960784 0.960784 0.960784; 1 1 0; 0.603922 0.803922 0.196078];
        i = find(strcmpi(nm, nms));
        if ~isempty(i); hex = clr(i, :);
        else; hex = [-1 -1 -1];
        end
    end
end
