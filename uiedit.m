classdef uiedit < matlab.mixin.SetGet %classdef uiedit < hgsetget % for < R2014b
% The ambition of UIEDIT is to make it a self-contained drop in replacement for
% uicontrol('Style', 'edit') with the unremoved power of the underlaying java
% controls. As it's far from complete there's a <a href="matlab:doc('uiedit/getJavaProp')">getJavaProp</a>(...) method and a
% corresponing <a href="matlab:doc('uiedit/setJavaProp')">setJavaProp</a>(prop, val, ...) as well as a <a href="matlab:doc('uiedit/javaCtrlFcn')">javaCtrlFcn</a>(...).
% The reason i started writing this was that i was missing a bunch of
% functionality in the standard control, as not all of which were native to
% javax.swing.JTextComponent either so i ended up with the following custom
% properties:
%	* <a href="matlab:doc('uiedit/AutoCompleteFcn')">AutoCompleteFcn</a>
%	* <a href="matlab:doc('uiedit/AutoCompleteStartLength')">AutoCompleteStartLength</a>
%	* <a href="matlab:doc('uiedit/NumbersOnly')">NumbersOnly</a>
%	* <a href="matlab:doc('uiedit/AutoSize')">AutoSize</a>
%	* <a href="matlab:doc('uiedit/UnwrappedCallbacks')">UnwrappedCallbacks</a>
%
% NOTES:
%	- Since setJavaProp sets any java-prop no-questions-asked it can mess
%	  up any and all of the built in functionality, use it with caution!
%	  same goes for <a href="matlab:doc('uiedit/javaCtrlFcn')">javaCtrlFcn</a>.
%	- 'KeyPressFcn' is an "alias" for 'KeyPressedCallback', as is
%	  'Callback' for 'ActionPerformedCallback'.
%	- Some props effect each other, e.g. setting Editable = false on a
%	  control with Enable = 'on' will change it to Enable = 'inactive'.
%
% DEVELOPER NOTES:
%	== Internal Callbacks ==
%	Don't mess with property internalcbs manually, if you add a new custom
%	property that uses an internal callback just use
%	obj.addInternalCallback('CallbackName', 'callbackfunction')
%	and obj.removeInternalCallback(<as add>) in your
%	set.PropThatUsesInternalCallbacks. They both return -1/0/1 for
%	no change/success/fail.
%	Eg:
%		obj.addInternalCallback('KeyReleasedCallback', ...
%								func2str(@obj.myreleasecb));
%	Note that your callbackfunction will recieve the jctrl handle and java
%	event plus any other functions that may have been assigned to that
%	callback. Thus your callback should be on the format (continuing on the
%	example above):
%		function myreleasecb(obj, h, e, varargin)
%			% do your thing here
%			if(nargin > 3); obj.nextcallback(h, e, varargin{:}); end
%		end
%	the last line is cruicial to not break the callback-chain. the reason i
%	did it this way is that the comparison "nargin > 3" (could also be
%	"~isempty(varagin)") is way faster than a function call in matlab and i
%	also believe that you should have the right to fuck things up.
%	== Timing Issues ==
%	Due to timing issues in java/matlab h.Text does not always contain
%	the entire string in h. i implented a quick-and-dirty workaround
%	(though it doesn't handle the case of two identical letters at the end
%	of the string) in "[s, sln] = strcbtimingfix(~, str, kc)". It's usually
%	called with "h.Text, e.getKeyChar" as args and returns the fixed string
%	and it's length.
%	== Utility functions ==
%	e = obj.seterr(~, propname)
%		generates error to throw if invalid value is given to a setter.
%		should be used whenever that is the case! example:
%			function set.MyCustomProp(obj, v)
%				if(~isvalid(v)); throw(obj.seterr('mycustomprop')); end
%				% do your thing here..
%				obj.MyCustomProp = v;
%			end
%	tf = obj.isposint(~, v, varargin);
%		checks if v is a positive integer.
%		optional args:
%		size			- allows vectors of length size, default 1
%		lowest_value	- value that v must be >= than/to, default 0
%		double_allowed	- if set ([]/{}/0 is also set!) doubles are allowed
%	tf = obj.isfhs(~, v)
%		checks if v is cellarray, returns false if not, and returns a
%		vector of booleans indicating which of the elements in that array
%		are function_handles.
%	v = jclr2vec(~, clr) and clr = vec2jclr(obj, v, propname)
%		converts java.awt.Color to matlab three element color vector
%		([r g b]) and back. the second one requires propname as it
%		generates and throws a seterr (see above) if vector is
%		malformatted.
%	[s, sln] = strcbtimingfix(~, str, kc)
%		see "Timing issues"
%
% TODO:
%	- The bug in NumbersOnly callback
%	- NumbersOnly/AutoCompleteFcn timing (now you have to set NumbersOnly
%	  first)
%	- Make sure NumbersOnly honors SelectionStart/End
%	- Documentation (look in help browser)
%	- Userdata with AutoCompleteFcn
%	- Multiline input
%	- Parse more events!!
%	- In-the-middle completion for AutoCompleteFcn
%	- Switch to ismembc (for speed) in NumbersOnly callback but first the
%	  numbersonlyarray needs to be sorted! ref:
%	  http://undocumentedmatlab.com/blog/ismembc-undocumented-helper-function
%	- NumbersOnly modes for hex, octal, binary
%	- Switch to http://www.mathworks.com/matlabcentral/fileexchange/30713-handlegraphicssetget-class
%	  as superclass and add the allowed values to props
%	- Everything below...
%
% See also
% uicontrol | <a href="matlab:doc('uicontrol_props')">uicontrol properties</a> | getJavaProp | setJavaProp

% This file is developed and maintained by Ragnar Seton
% contact: ragnar.seton@angstrom.uu.se
	properties
		%% == uicontrol('Style', 'edit') props == %%
		% notation: c/j    - hgjavacomponent/javax.swing.JTextField
		%					 property
		%			<prop> - corresponding property, if none given the
		%			         property is called the same thing

% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		BackgroundColor;		% j.Background
		%CData;
		
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Enable;					% j.Enabled and j.Editable
		%FontAngle;				% j.Font.<something>
		%FontName;				% "
		%FontSize;				% "
		%FontUnits;				% "
		%FontWeight;			% "
		
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		ForegroundColor;		% j.Foreground
		
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		HorizontalAlignment;	% j
		%ListboxTop;
		%Max;
		%Min;
		%SliderStep;
		%Value;					% c?
		
		%ButtonDownFcn;			% c
		%Children;				% c
		%Clipping;				% c
		%CreateFcn;				% c
		%DeleteFcn;				% c
		%BusyAction;			% c
		%HandleVisibility;		% c
		%HitTest;				% c
		%Interruptible;			% c
		%Selected;				% c
		%SelectionHighlight;	% c
		%UIContextMenu;			% c
		%UserData;				% c?
		
		%% == custom properties == %%

% Autocomplete callback
% Function called at each keystroke after AutoCompleteStartLength is
% reached, must take exactly two arguments (userdata not supported yet, on
% TODO), the current string and a direction indicator. The second
% indicates whether the user typed a character, in which case it's set to
% 0, pressed the down arrow key (then it's -1) or up arrow (1). The
% function must return exactly one value which should be the completed
% string. Note that in-the-middle completion is not suppoerted yet either
% i.e. the returned string must start with the current one.
		AutoCompleteFcn;			% j.KeyPressedCallback (wrapped)
% String length to start autocompletion
% Length at which to start autocompletion, if set AutoCompleteFcn will not
% be called before the string is longer than AutoCompleteStartLength.
% Default value is 2.
		AutoCompleteStartLength = 2;
% Numerical input only
% Only allow numerical input (including exponential notation). Possible
% values: {no}, yes, unsigned, integer, unsignedinteger
		NumbersOnly = 'no';			% "
	end
	properties(Dependent = true)
		%% == uicontrol('Style', 'edit') props == %%

% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Callback;				% j.ActionPerformedCallback
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		KeyPressFcn;			% j.KeyPressedCallback
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		String;					% j.Text
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		TooltipString;			% j.ToolTipText
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Units;					% c		
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Parent;					% c
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Position;				% c
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Tag;					% c
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Visible;				% c
		
		%% == wrapped java properties == %%

% See javax.swing.JComponent.setAutoscrolls
% <a href="http://docs.oracle.com/javase/7/docs/api/javax/swing/JComponent.html#setAutoscrolls(boolean)">javax.swing.JComponent.setAutoscrolls</a>
		Autoscrolls;
		%Border;
		
% See javax.swing.JTextComponent.setCaretPosition
% <a href="http://docs.oracle.com/javase/7/docs/api/javax/swing/text/JTextComponent.html#setCaretPosition(int)">javax.swing.JTextComponent.setCaretPosition</a>
		CaretPosition;
% See javax.swing.JTextComponent.setEditable
% <a href="http://docs.oracle.com/javase/7/docs/api/javax/swing/text/JTextComponent.html#setEditable(boolean)">javax.swing.JTextComponent.setEditable</a>
		Editable;
		
		%% == wrapped java callbacks == %%
		% the get/setters of these are placed last in this file since they
		% all look the same..

		MouseWheelMovedCallback;
		MouseClickedCallback;
		MouseEnteredCallback;
		MouseExitedCallback;
		MousePressedCallback;
		MouseReleasedCallback;
		CaretUpdateCallback;
		ComponentHiddenCallback;
		ComponentMovedCallback;
		ComponentResizedCallback;
		ComponentShownCallback;
		MouseDraggedCallback;
		MouseMovedCallback;
		ComponentAddedCallback;
		ComponentRemovedCallback;
		ActionPerformedCallback;
		AncestorResizedCallback;
		FocusGainedCallback;
		FocusLostCallback;
		HierarchyChangedCallback;
		CaretPositionChangedCallback;
		InputMethodTextChangedCallback;
		PropertyChangeCallback;
		AncestorMovedCallback;
		AncestorAddedCallback;
		AncestorRemovedCallback;
		KeyPressedCallback;
		KeyReleasedCallback;
		KeyTypedCallback;
		VetoableChangeCallback;
	end
	properties(SetAccess = private)
		%% == uicontrol('Style', 'edit') props == %%

% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		BeingDeleted;			% c
% See uicontrol properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
		Extent;					% j.PreferredSize
	end
	properties(SetAccess = immutable)
		%% == custom properties == %%

% Autosize control
% AutoSize is prioritized over 'Position' argument meaning you can set a
% x/y-pos and still have the control autosized. Possible values:
%	{no}   - Default/provided position is used
%	yes    - Control is sized to fit content (String)
%	width  - Control width is sized to fit content
%	height - Control height is sized to fit content
		AutoSize = 'no';
% Don't wrap callbacks
% If set to 'yes' callback-functions will not be wrapped and the handle and
% event arguments, if your callbacks receives them, are the java-ones.
% Callbacks that doesn't take arguments i.e. those given as single strings
% are never wrapped with the exception of KeyPressFcn/KeyPressedCallback
% when NumbersOnly or AutoCompleteFcn is set. Note that even if set to
% 'off' the java events are still available through the added field
% 'JavaEvent' in all event structures. Possible values: {no}, yes
		UnwrappedCallbacks = 'no';
	end
	properties(Constant = true)
		%% == uicontrol('Style', 'edit') props == %%

% Always 'edit'
% To facilitate drop in replacement of uicontrol('Style', 'edit')
		Style = 'edit';
% Always 'uicontrol'
% To facilitate drop in replacement of uicontrol('Style', 'edit')
		Type = 'uicontrol'; % should maybe be 'hgjavacomponent'.. ?
	end
	properties(Access = private, Hidden = true)
		%% == privates == %%
		jctrl;
		jcctrl;
		numbersonlyarray;
		internalcbs = {};
	end
	
	methods
		function obj = uiedit(varargin)
% See uicontrol and added properties
% <a href="matlab:doc('uicontrol_props')">uicontrol properties</a>
%
% See also:
% AutoSize | AutoCompleteFcn | AutoCompleteStartLength | NumbersOnly |
% UnwrappedCallbacks
			si = obj.strinca('Units', varargin);
			if(si ~= 0)
				vals = { 'pixels', 'normalized', 'inches', 'centimeters', ...
						 'points', 'characters' };
				if(~ischar(varargin{si + 1}) || ....
				   obj.strinca(varargin{si + 1}, vals) == 0)
					throw(obj.seterr('units'));
				end
				unts = lower(varargin{si + 1});
			else
				unts = 'pixels';
			end
			si = obj.strinca('Position', varargin);
			if(si ~= 0)
				if(~obj.isposint(varargin{si + 1}, 4, 0, 1))
					throw(obj.seterr('position'));
				end
				pos = varargin{si + 1};
				varargin(si : si + 1) = [];
			else
				pos = [20 20 60 20];
			end
			si = obj.strinca('Parent', varargin);
			if(si ~= 0)
				if(~ishghandle(varargin{si + 1}))
					throw(obj.seterr('parent'));
				end
				parent = varargin{si + 1};
				varargin(si : si + 1) = [];
			else
				parent = gcf();
			end
			[obj.jctrl, obj.jcctrl] = javacomponent(javax.swing.JTextField, ...
													pos, ...
													parent);
			obj.jcctrl = handle(obj.jcctrl);
			obj.jcctrl.Units = unts;
			obj.jcctrl.Position = pos;
			si = obj.strinca('String', varargin);
			if(si ~= 0)
				if(~ischar(varargin{si + 1}))
					throw(obj.seterr('string'));
				end
				obj.jctrl.setText(varargin{si + 1});
				varargin(si : si + 1) = [];
			end
			ps = obj.jctrl.getPreferredSize();
			obj.Extent = [0 0 ps.width ps.height];
			si = obj.strinca('AutoSize', varargin);
			if(si ~= 0)
				asvi = obj.strinca(varargin{si + 1}, ...
								   { 'yes', 'width', 'height', 'no' });
				if(asvi ~= 0)
					switch(asvi)
						case 1; pos(3:4) = [ps.width ps.height];
						case 2; pos(3:4) = [ps.width pos(4)];
						case 3; pos(3:4) = [pos(3) ps.height];
					end
					obj.jcctrl.Position = pos;
				else
					throw(obj.seterr('autosize'));
				end
				varargin(si : si + 1) = [];
			end
			si = obj.strinca('UnwrappedCallbacks', varargin);
			if(si ~= 0)
				if(strcmpi(varargin{si + 1}, 'yes'))
					obj.UnwrappedCallbacks = 'yes';
				elseif(~strcmpi(varargin{si + 1}, 'no'))
					throw(obj.seterr('unwrappedcallbacks'));
				end
			end
			% just shut up and ignore it..
			si = obj.strinca('Style', varargin);
			if(si ~= 0); varargin(si : si + 1) = []; end
			for i = 1 : 2 : length(varargin)
				set(obj, varargin{i}, varargin{i + 1});
			end
		end
		% this should be handled when it's BeingDeleted
% 		function delete(obj)
% 			% this seems to be different on different platforms and/or
% 			% versions of java..
% 			if(ishandle(obj.jctrl))
% 				% we should clear all callbacks here..
% 				if(~strcmp(obj.NumbersOnly, 'off'))
% 					obj.jctrl.KeyPressedCallback = [];
% 				end
% 				delete(obj.jctrl);
% 			end
% 			if(ishandle(obj.jcctrl)); delete(obj.jcctrl); end
% 		end
		function v = getJavaProp(obj, varargin)
% Fetch java property
% Acts on the underlaying javax.swing.JTextComponent
			if(nargin == 1)
				v = get(obj.jctrl);
			else
				v = cell(nargin - 1, 2);
				for i = 1 : nargin - 1
					if(~ischar(varargin{i}))
						error('daqs:uiedit:getjavaprop:invalidpropertyname', ...
							  'Argument %u is not a property name.', i);
					else
						v{i, 1} = varargin{i};
						v{i, 2} = obj.jctrl.(varargin{i});
					end
				end
			end
		end
		function setJavaProp(obj, varargin)
% Fetch java property
% Acts on the underlaying javax.swing.JTextComponent
			if(nargin == 1 || mod(nargin - 1, 2) ~= 0)
				error('daqs:uiedit:setjavaprop:invalidparamvaluepair', ...
					  'Invalid parameter/value pair arguments.');
			end
			for i = 1 : 2 : nargin - 1
				if(~ischar(varargin{i}))
					error('daqs:uiedit:setjavaprop:invalidpropertyname', ...
						  'Parameter %u is not a property name.', i + 1);
				else
					obj.jctrl.(['set', varargin{i}])(varargin{i + 1});
				end
			end
		end
		%% == uicontrol('Style', 'edit') props == %%
		function v = get.BackgroundColor(obj)
			if(isempty(obj.BackgroundColor))
				obj.BackgroundColor = obj.jclr2vec(obj.jctrl.Background);
			end
			v = obj.BackgroundColor;
		end
		function set.BackgroundColor(obj, v)
			clr = obj.vec2jclr(v, 'backgroundcolor');
			obj.jctrl.setBackground(clr); %#ok<*MCSUP>
			obj.BackgroundColor = obj.jclr2vec(clr);
		end
% TODO:
%CData;
		function v = get.Callback(obj)
			v = obj.getcallback('ActionPerformedCallback');
		end
		function set.Callback(obj, v)
			obj.setcallback('ActionPerformedCallback', v);
		end
		function v = get.Enable(obj)
			if(isempty(obj.Enable))
				if(~obj.jctrl.Enable); obj.Enable = 'off';
				elseif(~obj.jctrl.Editable); obj.Enable = 'inactive';
				else; obj.Enable = 'on';
				end
			end
			v = obj.Enable;
		end
		function set.Enable(obj, v)
			if(~ischar(v)); throw(obj.seterr('enable')); end
			lv = lower(v);
			switch(lv)
				case 'off'; obj.jctrl.setEnabled(false);
				case 'on'; obj.jctrl.setEnabled(true); 
				case 'inactive'; obj.jctrl.setEditable(false);
				otherwise; throw(obj.seterr('enable'));
			end
			obj.Enable = lv;
		end
% TODO:
%FontAngle;				% j.Font
%FontName;				% "
%FontSize;				% "
%FontUnits;				% "
%FontWeight;			% "
		function v = get.ForegroundColor(obj)
			if(isempty(obj.ForegroundColor))
				obj.ForegroundColor = obj.jclr2vec(obj.jctrl.Foreground);
			end
			v = obj.ForegroundColor;
		end
		function set.ForegroundColor(obj, v)
			clr = obj.vec2jclr(v, 'foregroundcolor');
			obj.jctrl.setForeground(clr);
			obj.ForegroundColor = obj.jclr2vec(clr);
		end
		function v = get.HorizontalAlignment(obj)
			if(isempty(obj.HorizontalAlignment))
				switch(obj.jctrl.HorizontalAlignment)
					case javax.swing.JTextField.LEFT
						obj.HorizontalAlignment = 'left';
					case javax.swing.JTextField.CENTER
						obj.HorizontalAlignment = 'center';
					case javax.swing.JTextField.RIGHT
						obj.HorizontalAlignment = 'right';
					otherwise % lets use left alignment as default..
						obj.jctrl.setHorizontalAlignment(javax.swing.JTextField.LEFT);
						obj.HorizontalAlignment = 'left';
				end
			end
			v = obj.HorizontalAlignment;
		end
		function set.HorizontalAlignment(obj, v)
			if(~ischar(v)); throw(obj.seterr('horizontalalignment')); end
			lv = lower(v);
			switch(lv)
				case 'left'
					obj.jctrl.setHorizontalAlignment(javax.swing.JTextField.LEFT);
				case 'center'
					obj.jctrl.setHorizontalAlignment(javax.swing.JTextField.CENTER);
				case 'right'
					obj.jctrl.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
				otherwise
					throw(obj.seterr('horizontalalignment'));
			end
			obj.HorizontalAlignment = lv;
		end
		function v = get.KeyPressFcn(obj)
			v = obj.getcallback('KeyPressedCallback');
		end
		function set.KeyPressFcn(obj, v)
			obj.setcallback('KeyPressedCallback', v);
		end
% TODO:
%ListboxTop;
%Max;
%Min;
		function v = get.Position(obj)
			v = obj.jcctrl.Position;
		end
		function set.Position(obj, v)
			if(~obj.isposint(v, 4, 0, 1))
				throw(obj.seterr('position'));
			end
			obj.jcctrl.Position = v;
		end
		function v = get.String(obj)
			v = obj.jctrl.Text;
		end
		function set.String(obj, v)
			if(~ischar(v)); throw(obj.seterr('string')); end
			obj.jctrl.setText(v);
		end
% TODO:
%SliderStep;
		function v = get.TooltipString(obj)
			v = obj.jctrl.ToolTipText;
		end
		function set.TooltipString(obj, v)
			if(~ischar(v)); throw(obj.seterr('tooltipstring')); end
			obj.jctrl.setToolTipText(v);
		end
		function v = get.Units(obj)
			v = obj.jcctrl.Units;
		end
		function set.Units(obj, v)
			vals = { 'pixels', 'normalized', 'inches', 'centimeters', ...
					 'points', 'characters' };
			if(~ischar(v) || obj.strinca(v, vals) == 0)
				throw(obj.seterr('units'));
			end
			obj.jcctrl.Units = lower(v);
		end
% TODO:
%Value;
		function v = get.BeingDeleted(obj)
			v = obj.jcctrl.BeingDeleted;
		end
		function v = get.Extent(obj)
			ps = obj.jctrl.getPreferredSize();
			v = [0 0 ps.width ps.height];
		end
% TODO:
%ButtonDownFcn;			% c
%Children;				% c
%Clipping;				% c
%CreateFcn;				% c
%DeleteFcn;				% c
%BusyAction;			% c
%HandleVisibility;		% c
%HitTest;				% c
%Interruptible;			% c
		function v = get.Parent(obj)
			v = obj.jcctrl.Parent;
		end
		function set.Parent(obj, v)
			if(~ishghandle(v)); throw(obj.seterr('parent')); end
			obj.jcctrl.Parent = v;
		end
% TODO:
%Selected;				% c
%SelectionHighlight;	% c
		function v = get.Tag(obj)
			v = obj.jcctrl.Tag;
		end
		function set.Tag(obj, v)
			if(~ischar(v)); throw(obj.seterr('tag')); end
			obj.jcctrl.Tag = v;
		end
%UIContextMenu;			% c
%UserData;				% c?
		function v = get.Visible(obj)
			v = obj.jcctrl.Visible;
		end
		function set.Visible(obj, v)
			vals = { 'on', 'off' };
			if(~ischar(v) || obj.strinca(v, vals) == 0)
				throw(obj.seterr('visible'));
			end
			obj.jcctrl.Visible = lower(v);
		end
		
		%% == wrapped java properties == %%
		function v = get.Autoscrolls(obj)
			v = obj.jctrl.Autoscrolls;
		end
		function set.Autoscrolls(obj, v)
			if(~islogical(v)); throw(obj.seterr('autoscrolls')); end
			obj.jctrl.setAutoscrolls(v);
		end
% TODO:
%Border;				% j
		function v = get.CaretPosition(obj)
			v = obj.jctrl.CaretPosition;
		end
		function set.CaretPosition(obj, v)
			if(~obj.isposint(v) || v > length(obj.jctrl.Text))
				throw(obj.seterr('caretposition'));
			end
			obj.jctrl.setCaretPosition(v);
		end
		function v = get.Editable(obj)
			v = obj.jctrl.Editable;
		end
		function set.Editable(obj, v)
			if(~islogical(v)); throw(obj.seterr('Editable')); end
			obj.jctrl.setEditable(v);
			if(v); obj.Enable = 'on';
			elseif(strcmp(obj.Enable, 'on')); obj.Enable = 'inactive';
			end
		end
		
		%% == custom properties == %%
		function v = get.AutoCompleteFcn(obj)
			v = obj.AutoCompleteFcn;
		end
		function set.AutoCompleteFcn(obj, v)
			if(~isa(v, 'function_handle') && ~isempty(v))
				throw(obj.seterr('autocompletefcn'));
			end
			cbn = 'KeyPressedCallback';
			fn = func2str(@obj.autocompletecb);
			if(isempty(v))
				res = obj.removeInternalCallback(cbn, fn);
			else
				res = obj.addInternalCallback(cbn, fn);
			end
			if(res > 0)
				if(isempty(v)); s = '<none>';
				else; s = func2str(v);
				end
				warning('daqs:uiedit:setautocompletefcn', ...
						'Setting AutoCompleteFcn to ''%s'' failed, no changes have been made.', ...
						s);
			else
				obj.AutoCompleteFcn = v;
			end
		end
		function v = get.AutoCompleteStartLength(obj)
			v = obj.AutoCompleteStartLength;
		end
		function set.AutoCompleteStartLength(obj, v)
			if(~obj.isposint(v, 1, 1))
				throw(obj.seterr('autocompletestartlength'));
			end
			obj.AutoCompleteStartLength = v;
		end
		function v = get.NumbersOnly(obj)
			v = obj.NumbersOnly;
		end
		function set.NumbersOnly(obj, v)
			if(~ischar(v)); throw(obj.seterr('numbersonly')); end
			lv = lower(v);
			onoa = obj.numbersonlyarray;
			switch(lv)
				case 'no'
					obj.numbersonlyarray = '';
				case 'yes'
					obj.numbersonlyarray = '-eE.0123456789';
				case 'unsigned'
					obj.numbersonlyarray = 'eE.0123456789';
				case 'integer'
					obj.numbersonlyarray = '-eE0123456789';
				case 'unsignedinteger'
					obj.numbersonlyarray = 'eE0123456789';
				otherwise
					throw(obj.seterr('numbersonly'));
			end
			cbn = 'KeyPressedCallback';
			fn = func2str(@obj.numbersonlycb);
			if(isempty(obj.numbersonlyarray))
				res = obj.removeInternalCallback(cbn, fn);
			else
				res = obj.addInternalCallback(cbn, fn);
			end
			if(res > 0)
				obj.numbersonlyarray = onoa;
				warning('daqs:uiedit:setnumbersonly', ...
						'Setting NumbersOnly to ''%s'' failed, no changes have been made.', ...
						lv);
			else
				obj.NumbersOnly = lv;
			end
		end
	end
	%% == internal shit that you may have seen above.. == %%
	methods(Hidden = true)
		function dbg(obj)
			for i = 1 : 2 : numel(obj.internalcbs)
				fprintf('CB: %s\nFN(s): ', obj.internalcbs{i});
				disp(obj.internalcbs{i + 1});
			end
			fprintf('noa: ''%s''\n', obj.numbersonlyarray);
		end
		function idx = strinca(~, s, ca)
			idx = 0;
			for i = 1 : length(ca)
				if (isa(ca{i}, 'char') && strcmpi(s, ca{i}))
					idx = i;
					break;
				end
			end
		end
 		function callbackwrapper(obj, ~, e, cb, varargin)
			bm = e.getModifiers();
			if(bm == 0) % since field with {}-content clears whole struct
				mods = {''};
			else
				allmods = {'alt', 'command', 'control', 'shift'};
				mods = allmods([bitand(bm, 8) ~= 0, bitand(bm, 4) ~= 0, ...
								bitand(bm, 2) ~= 0, bitand(bm, 1) ~= 0]);
			end
			if(~isa(cb, 'function_handle') && ~ischar(cb))
				% huston, we have problem
% TODO: do something..
			else
				if(ischar(cb))
					cb = str2func(cb);
					if(nargin == 4) % string only callback
						cb();
						return;
					end
				end
				switch(class(e))
					case 'java.awt.event.KeyEvent'
						cbe = struct('Character', e.getKeyChar(), ...
									 'Modifier', mods, ...
									 'Key', sprintf('%c', e.getKeyCode()), ...
									 'JavaEvent', e);
					case 'java.awt.event.MouseEvent'
						ssz = get(0, 'ScreenSize');
						cbe = struct('Point', [e.getX(), ...
											   obj.Position(4) - e.getY()], ...
									 'Modifier', mods, ...
									 'LocationOnScreen', [e.getXOnScreen(), ...
														  ssz(4) - e.getYOnScreen()], ...
									 'JavaEvent', e);
					case 'java.awt.event.ActionEvent'
						cbe = struct('JavaEvent', e);
% TODO: parse more events..
					otherwise
						cbe = e;
				end
				cb(obj, cbe, varargin{:});
			end
  		end
		function autocompletecb(obj, h, e, varargin)
			persistent str; persistent sln;
			strcb = obj.AutoCompleteFcn;
			if(~isa(strcb, 'function_handle'))
				warning('daqs:uidedit:autocompletecb:incstate', ...
						'Inconsistent internal state found: no autcomplete string function found.');
			else
				kch = e.getKeyChar();
				dir = 0;
				% skip for command and ctrl mods and anything below space
				if(bitand(e.getModifiers(), 6) == 0 && ...
				   31 < kch && kch < 65535)
					[str, sln] = obj.strcbtimingfix(h.Text, kch);
					if(sln < obj.AutoCompleteStartLength); return; end
				else % modifiers are ok with arrow keys
					kc = e.getKeyCode();
					if(kc == java.awt.event.KeyEvent.VK_DOWN || ...
					   kc == java.awt.event.KeyEvent.VK_KP_DOWN)
						dir = -1;
					elseif(kc == java.awt.event.KeyEvent.VK_UP || ...
						   kc == java.awt.event.KeyEvent.VK_KP_UP)
						dir = 1;
					else
						return;
					end
				end
				newstr = strcb(str, dir);
				% in-the-middle completion not supported yet
				if(ischar(newstr) && ...
				   length(newstr) >= length(str) && ...
				   strncmpi(newstr, str, length(str)))
					h.setText([str, newstr(sln + 1 : end)]);
					h.setSelectionStart(sln);
					h.setSelectionEnd(length(newstr));
				end
			end
			if(nargin > 3); obj.nextcallback(h, e, varargin{:}); end
		end
		function numbersonlycb(obj, h, e, varargin)
			% bug: pasting strings with multiple -'s (don't have time atm)
%			kc = e.getKeyChar();
%			[str, sln] = obj.strcbtimingfix(h.Text, kc);
			str = h.Text;
			kc = e.getKeyCode();
			switch(kc)
				case java.awt.event.KeyEvent.VK_UP
					h.setCaretPosition(0);
					return;
				case java.awt.event.KeyEvent.VK_DOWN
					h.setCaretPosition(numel(str));
					return;
				case java.awt.event.KeyEvent.VK_LEFT; return;
				case java.awt.event.KeyEvent.VK_RIGHT; return;
			end
			if(numel(str) > 0 && e.getModifiers() < 2 && kc > 31)
				oldstr = str;
				str(~ismember(str, obj.numbersonlyarray)) = '';
				if(~strcmp(oldstr, str))
					cp = h.CaretPosition;
					h.setText(str);
					h.setCaretPosition(min(max(cp - 1, 0), numel(str)));
				else
					[st, en] = regexpi(str, ...
									   '^\-?((\d+((\.\d*)|((\.\d+)?((e\-)|e)?\d*)))|(\d*))$', ...
									   'once');
					if(isempty(st) || (st ~= 1 || en ~= numel(str)))
	 					cp = max(h.CaretPosition, 1);
						h.setText(str([1 : cp - 1, cp + 1 : end]));
						h.setCaretPosition(max(cp - 1, 0));
					end
				end
			end
			if(nargin > 3); obj.nextcallback(h, e, varargin{:}); end
		end
		function v = jclr2vec(~, clr)
			v = [clr.getRed(), clr.getGreen(), clr.getBlue] ./ 255;
		end
		function clr = vec2jclr(obj, v, propname)
			if(~ischar(v) || ~obj.isposint(v, 3, 0, 1) || any(v > 1))
				throw(obj.seterr(propname));
			end
			if(ischar(v))
				switch(v(1))
					case 'r'; clr = java.awt.Color.RED;
					case 'g'; clr = java.awt.Color.GREEN;
					case 'b'
						if(length(v) == 1 || strcmpi(v, 'blue'))
							clr = java.awt.Color.BLUE;
						elseif(strcmpi(v, 'black'))
							clr = java.awt.Color.BLACK;
						else
							throw(obj.seterr(propname));
						end
					case 'c'; clr = java.awt.Color.CYAN;
					case 'm'; clr = java.awt.Color.MAGENTA;
					case 'y'; clr = java.awt.Color.YELLOW;
					case 'k'; clr = java.awt.Color.BLACK;
					case 'w'; clr = java.awt.Color.WHITE;
					otherwise; throw(obj.seterr(propname));
				end
			else
				clr = java.awt.Color(v(1), v(2), v(3));
			end
		end
	end
	%% == super duper internal shit == %%
	methods(Access = private, Hidden = true)
		function e = seterr(~, propname)
			e = MException(['daqs:uiedit:set:', propname], ...
						   ['Bad property value found.\n', ...
							'Object Name: uiedit\n', ...
							'Property Name: ''%s'''], ...
						   propname);
		end
		function [s, sln] = strcbtimingfix(~, str, kc)
			s = str;
			sln = numel(str);
			% timing issues..
			if(sln > 0 && s(sln) ~= kc)
				s = [str, kc];
				sln = sln + 1;
			end
		end
		function cb = getcallback(obj, fn)
			cbw = obj.jctrl.(fn);
			if(isempty(cbw))
				cb = cbw;
				return;
			end
			cbi = obj.strinca(fn, obj.internalcbs);
			if(cbi ~= 0)
				if(iscell(cbw))
					for i = numel(cbw) : -1 : 1
						if(isa(cbw{i}, 'function_handle') && ...
						   obj.strinca(func2str(cbw{i}), obj.internalcbs{cbi + 1}) ~= 0)
							cbw(i) = [];
						end
					end
					if(isempty(cbw)); cbw = [];
					elseif(numel(cbw) == 1); cbw = cbw{1};
					end
				elseif(isa(cbw, 'function_handle') && ...
					   obj.strinca(func2str(cbw), obj.internalcbs{cbi + 1}) ~= 0)
					cbw = [];
				end
			end
			if(obj.UnwrappedCallbacks(1) == 'y' || ~iscell(cbw)); cb = cbw;
			elseif(length(cbw) == 2); cb = cbw{2};
			else; cb = cbw(2 : end);
			end
		end
		function setcallback(obj, fn, cb)
			if(obj.UnwrappedCallbacks(1) == 'y' || isempty(cb) || ...
			   ischar(cb))
				cbw = cb;
			elseif(isa(cb, 'function_handle'))
				cbw = { @obj.callbackwrapper, cb };
			elseif(iscell(cb))
				cbw = [ { @obj.callbackwrapper }, cb ];
			else
				throw(obj.seterr(fn));
			end
			cbi = obj.strinca(fn, obj.internalcbs);
			if(cbi ~= 0); cbw = obj.wrapInternalCallbacks(cbw, cbi); end
			obj.jctrl.(fn) = cbw;
		end
		function nextcallback(~, h, e, varargin)
			if(ischar(varargin{1}))
				cb = str2func(varargin{1});
			elseif(isa(varargin{1}, 'function_handle'))
				cb = varargin{1};
			else % got dammit huston! we have a problem here!
% TODO: do something..
				return;
			end
			if(nargin > 4); cb(h, e, varargin{2 : end}); % wrapped
			elseif(ischar(varargin{1})); cb();			 % string
			else; cb(h, e);								 % unwrapped
			end
		end
		function cbw = wrapInternalCallbacks(obj, cb, cbi)
			if(iscell(cb))
				% since internal cbs are all function_handles we only care
				% about current ones that are
				ccbs = cellfun(@(c)func2str(c), cb(obj.isfhs(cb)), ...
								'UniformOutput', false);
			elseif(isa(cb, 'function_handle'))
				ccbs = { func2str(cb) };
			else
				ccbs = {};
			end
			% check if any new internal cb is being added
			icbs = cell(1, numel(obj.internalcbs{cbi + 1}));
			j = 1;
			for i = 1 : numel(icbs)
				if(obj.strinca(obj.internalcbs{cbi + 1}{i}, ccbs) == 0)
					% str2func craps out.. luckily eval works.
					icbs{j} = eval(obj.internalcbs{cbi + 1}{i});
					j = j + 1;
				end
			end
			icbs(j : end) = [];
			if(~isempty(icbs))
				% [ @fn1, { @fn2, @fn3 } ] doesn't work..
				if(isempty(cb))
					if(numel(icbs) == 1); cbw = icbs{1};
					else; cbw = icbs;
					end
				elseif(iscell(cb))
					cbw = [ icbs, cb ];
				else
					cbw = [ icbs, { cb } ];
				end
			else
				cbw = cb;
			end
		end
		function ret = addInternalCallback(obj, cbn, fn)
			% make sure it's a valid callbackname
			try cb = obj.jctrl.(cbn);
			catch e
				warning('daqs:uiedit:addinternalcallback:invalidname', ...
						'Failed to add internal callback.\n%s', ...
						getReport(e));
				ret = 1;
				return;
			end
			% add cb name and/or function to internalcbs if not present
			cbi = obj.strinca(cbn, obj.internalcbs);
			if(cbi == 0)
				obj.internalcbs = { obj.internalcbs{:}, cbn, { fn } }; %#ok<CCAT>
				cbi = numel(obj.internalcbs) - 1;
				ret = 0;
			else
				fi = obj.strinca(fn, obj.internalcbs{cbi + 1});
				if(fi == 0)
					obj.internalcbs{cbi + 1} = ...
						[ obj.internalcbs{cbi + 1}, fn ];
					ret = 0;
				else
					ret = -1;
				end
			end
			% update callback if new
			if(ret == 0)
				obj.jctrl.(cbn) = obj.wrapInternalCallbacks(cb, cbi);
			end
		end
		function ret = removeInternalCallback(obj, cbn, fn)
			% make sure it's present..
			ret = -1;
			cbi = obj.strinca(cbn, obj.internalcbs);
			if(cbi == 0); return; end
			fi = obj.strinca(fn, obj.internalcbs{cbi + 1});
			if(fi == 0); return; end
			ret = 0;
			% ..and that it's a valid cb name
			try cb = obj.jctrl.(cbn);
			catch e
				warning('daqs:uiedit:removeinternalcallback:invalidname', ...
						'Failed to remove internal callback.\n%s', ...
						getReport(e));
				ret = 1;
				return;
			end
			% remove the function from the callback (cell) array
			if(~isempty(cb) && iscell(cb))
				for i = 1 : numel(cb)
					if(isa(cb{i}, 'function_handle') && ...
						strcmpi(func2str(cb{i}), fn))
						cb(i) = [];
						i = 0; %#ok<FXSET>
						break;
					end
				end
				% this really shouldn't happen..
				if(i ~= 0); ret = -1; end
			elseif(isa(cb, 'function_handle') && strcmpi(func2str(cb), fn))
				cb = [];
			else % ..neither should this
				ret = -1;
			end
			if(ret == -1)
				warning('daqs:uidedit:removeinternalcallabck:incstate', ...
						'Inconsistent internal state found: internal callback found in cell array but not in function list.');
			end
			% since we have cbi & fi 
			obj.internalcbs{cbi + 1}(fi) = [];
			if(numel(obj.internalcbs{cbi + 1}) == 0)
				obj.internalcbs(cbi : cbi + 1) = [];
			end
			% this should always be the case here..
			if(ret == 0); obj.jctrl.(cbn) = cb; end
		end
		function tf = isfhs(~, v)
			if(iscell(v)); tf = cellfun(@(c)isa(c, 'function_handle'), v);
			else; tf = false;
			end
		end
		function tf = isposint(~, v, varargin)
		% optional args: size, lowest_value, double_allowed (only isset!)
			sz = 1;
			lv = 0;
			if(nargin > 2)
				sz = varargin{1};
				if(nargin > 3); lv = varargin{2}; end
			end
			tf = ( isnumeric(v) && ...
				   isreal(v) && ...
				   length(v) == sz && ...
				   all(v >= lv) && ...
				   ~any(isnan(mod(v, 1))) );
			if(nargin < 5); tf = tf && ~any(mod(v, 1)); end
		end
	end
	%% == wrapped java callbacks == %%
	methods
		function v = get.MouseWheelMovedCallback(obj)
			v = obj.getcallback('MouseWheelMovedCallback');
		end
		function set.MouseWheelMovedCallback(obj, v)
			obj.setcallback('MouseWheelMovedCallback', v);
		end
		function v = get.MouseClickedCallback(obj)
			v = obj.getcallback('MouseClickedCallback');
		end
		function set.MouseClickedCallback(obj, v)
			obj.setcallback('MouseClickedCallback', v);
		end
		function v = get.MouseEnteredCallback(obj)
			v = obj.getcallback('MouseEnteredCallback');
		end
		function set.MouseEnteredCallback(obj, v)
			obj.setcallback('MouseEnteredCallback', v);
		end
		function v = get.MouseExitedCallback(obj)
			v = obj.getcallback('MouseExitedCallback');
		end
		function set.MouseExitedCallback(obj, v)
			obj.setcallback('MouseExitedCallback', v);
		end
		function v = get.MousePressedCallback(obj)
			v = obj.getcallback('MousePressedCallback');
		end
		function set.MousePressedCallback(obj, v)
			obj.setcallback('MousePressedCallback', v);
		end
		function v = get.MouseReleasedCallback(obj)
			v = obj.getcallback('MouseReleasedCallback');
		end
		function set.MouseReleasedCallback(obj, v)
			obj.setcallback('MouseReleasedCallback', v);
		end
		function v = get.CaretUpdateCallback(obj)
			v = obj.getcallback('CaretUpdateCallback');
		end
		function set.CaretUpdateCallback(obj, v)
			obj.setcallback('CaretUpdateCallback', v);
		end
		function v = get.ComponentHiddenCallback(obj)
			v = obj.getcallback('ComponentHiddenCallback');
		end
		function set.ComponentHiddenCallback(obj, v)
			obj.setcallback('ComponentHiddenCallback', v);
		end
		function v = get.ComponentMovedCallback(obj)
			v = obj.getcallback('ComponentMovedCallback');
		end
		function set.ComponentMovedCallback(obj, v)
			obj.setcallback('ComponentMovedCallback', v);
		end
		function v = get.ComponentResizedCallback(obj)
			v = obj.getcallback('ComponentResizedCallback');
		end
		function set.ComponentResizedCallback(obj, v)
			obj.setcallback('ComponentResizedCallback', v);
		end
		function v = get.ComponentShownCallback(obj)
			v = obj.getcallback('ComponentShownCallback');
		end
		function set.ComponentShownCallback(obj, v)
			obj.setcallback('ComponentShownCallback', v);
		end
		function v = get.MouseDraggedCallback(obj)
			v = obj.getcallback('MouseDraggedCallback');
		end
		function set.MouseDraggedCallback(obj, v)
			obj.setcallback('MouseDraggedCallback', v);
		end
		function v = get.MouseMovedCallback(obj)
			v = obj.getcallback('MouseMovedCallback');
		end
		function set.MouseMovedCallback(obj, v)
			obj.setcallback('MouseMovedCallback', v);
		end
		function v = get.ComponentAddedCallback(obj)
			v = obj.getcallback('ComponentAddedCallback');
		end
		function set.ComponentAddedCallback(obj, v)
			obj.setcallback('ComponentAddedCallback', v);
		end
		function v = get.ComponentRemovedCallback(obj)
			v = obj.getcallback('ComponentRemovedCallback');
		end
		function set.ComponentRemovedCallback(obj, v)
			obj.setcallback('ComponentRemovedCallback', v);
		end
		function v = get.ActionPerformedCallback(obj)
			v = obj.getcallback('ActionPerformedCallback');
		end
		function set.ActionPerformedCallback(obj, v)
			obj.setcallback('ActionPerformedCallback', v);
		end
		function v = get.AncestorResizedCallback(obj)
			v = obj.getcallback('AncestorResizedCallback');
		end
		function set.AncestorResizedCallback(obj, v)
			obj.setcallback('AncestorResizedCallback', v);
		end
		function v = get.FocusGainedCallback(obj)
			v = obj.getcallback('FocusGainedCallback');
		end
		function set.FocusGainedCallback(obj, v)
			obj.setcallback('FocusGainedCallback', v);
		end
		function v = get.FocusLostCallback(obj)
			v = obj.getcallback('FocusLostCallback');
		end
		function set.FocusLostCallback(obj, v)
			obj.setcallback('FocusLostCallback', v);
		end
		function v = get.HierarchyChangedCallback(obj)
			v = obj.getcallback('HierarchyChangedCallback');
		end
		function set.HierarchyChangedCallback(obj, v)
			obj.setcallback('HierarchyChangedCallback', v);
		end
		function v = get.CaretPositionChangedCallback(obj)
			v = obj.getcallback('CaretPositionChangedCallback');
		end
		function set.CaretPositionChangedCallback(obj, v)
			obj.setcallback('CaretPositionChangedCallback', v);
		end
		function v = get.InputMethodTextChangedCallback(obj)
			v = obj.getcallback('InputMethodTextChangedCallback');
		end
		function set.InputMethodTextChangedCallback(obj, v)
			obj.setcallback('InputMethodTextChangedCallback', v);
		end
		function v = get.PropertyChangeCallback(obj)
			v = obj.getcallback('PropertyChangeCallback');
		end
		function set.PropertyChangeCallback(obj, v)
			obj.setcallback('PropertyChangeCallback', v);
		end
		function v = get.AncestorMovedCallback(obj)
			v = obj.getcallback('AncestorMovedCallback');
		end
		function set.AncestorMovedCallback(obj, v)
			obj.setcallback('AncestorMovedCallback', v);
		end
		function v = get.AncestorAddedCallback(obj)
			v = obj.getcallback('AncestorAddedCallback');
		end
		function set.AncestorAddedCallback(obj, v)
			obj.setcallback('AncestorAddedCallback', v);
		end
		function v = get.AncestorRemovedCallback(obj)
			v = obj.getcallback('AncestorRemovedCallback');
		end
		function set.AncestorRemovedCallback(obj, v)
			obj.setcallback('AncestorRemovedCallback', v);
		end
		function v = get.KeyPressedCallback(obj)
			v = obj.getcallback('KeyPressedCallback');
		end
		function set.KeyPressedCallback(obj, v)
			obj.setcallback('KeyPressedCallback', v);
		end
		function v = get.KeyReleasedCallback(obj)
			v = obj.getcallback('KeyReleasedCallback');
		end
		function set.KeyReleasedCallback(obj, v)
			obj.setcallback('KeyReleasedCallback', v);
		end
		function v = get.KeyTypedCallback(obj)
			v = obj.getcallback('KeyTypedCallback');
		end
		function set.KeyTypedCallback(obj, v)
			obj.setcallback('KeyTypedCallback', v);
		end
		function v = get.VetoableChangeCallback(obj)
			v = obj.getcallback('VetoableChangeCallback');
		end
		function set.VetoableChangeCallback(obj, v)
			obj.setcallback('VetoableChangeCallback', v);
		end
	end
end
