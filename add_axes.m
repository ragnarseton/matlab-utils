function varargout = add_axes(ah, opts)
% Use this if you essentially want to copy the axes (same ticks and whatnot) of
% a plot 
	arguments
		ah matlab.graphics.axis.Axes
		opts.hide_x {mustBeNumericOrLogical} = false
		opts.hide_y {mustBeNumericOrLogical} = false
		opts.x_loc {mustBeMember(opts.x_loc, {'top', 'bottom'})} = 'top'
		opts.y_loc {mustBeMember(opts.y_loc, {'left', 'right'})} = 'right'
		opts.x_props cell {mustBeVector(opts.x_props, "allow-all-empties")} = {}
		opts.y_props cell {mustBeVector(opts.y_props, "allow-all-empties")} = {}
	end
	nah = axes('Parent', ah.Parent, 'Position', ah.Position, ...
			   'XAxisLocation', opts.x_loc, 'YAxisLocation', opts.y_loc, ...
			   'Color', 'none');
	nah.XAxis.Visible = ~opts.hide_x;
	nah.YAxis.Visible = ~opts.hide_y;
%	if ah.Box == matlab.lang.OnOffSwitchState.on
%		set([nah.XAxis, nah.YAxis], 'Color', 'none');
%	end
	alps = ["Limits", "TickValues"];
	oneway_linkprop([ah.XAxis, nah.XAxis], alps);
	oneway_linkprop([ah.YAxis, nah.YAxis], alps);
	lps = ["Units", "Position", "FontSize", "FontName", "FontWeight", ...
		   "FontUnits"];
	oneway_linkprop([ah, nah], lps);
	if ~isempty(opts.x_props); set(nah.XAxis, opts.x_props{:}); end
	if ~isempty(opts.y_props); set(nah.YAxis, opts.y_props{:}); end
	if nargout > 0
		if nargout == 1
			varargout{1} = nah;
		elseif nargout == 2
			varargout{1} = nah.XAxis;
			varargout{2} = nah.YAxis;
		elseif nargout == 3
			varargout{1} = nah;
			varargout{2} = nah.XAxis;
			varargout{3} = nah.YAxis;
		end
	end
	function oneway_linkprop(o, lps, varargin)
		if nargin > 2; app = varargin{1};
		else; app = o(1);
		end
		% copies the props from the first to all the rest before linking
		for p = lps
			for i = 2:numel(o); o(i).(p) = o(1).(p); end
		end
		hdls = linkprop(o, lps);
		% this is to ensure the link stays alive as long as the parent of the
		% first element exists (a bit aritrary, but probably ok for most cases).
		% see https://undocumentedmatlab.com/articles/using-linkaxes-vs-linkprop
		% for reference. this fixes, for example, the issue of the links being
		% lost when the parent gets new children.
		setappdata(app, 'PropListeners', hdls);
	end
end