function visual_data_editor(dat)
% quick'n dirty "data editor" to be able to delete, i.e. set to nan, sections of
% a matrix.
% TODO:
% - add a dropdown with shapes rather than just a rect
% - make value to set to selectable
% - make value to use as transparent selectable
% - possibly an undo
	f = figure('Name', 'Visual data editor');
	ah = axes('Parent', f, 'Units', 'norm');
	% TODO: handle dims > 2
	img = imagesc(ah, dat, 'AlphaData', ~isnan(dat));
	r = images.roi.Rectangle('Deletable', false, 'FaceAlpha', 0, ...
							 'Position', [1,1,size(dat,2)/2,size(dat,1)/2], ...
							 'Color', 'b', 'Parent', ah, 'LineWidth', 1, ...
							 'MarkerSize', 5);
	ah_p = ah.Position;
	uicontrol('Parent', f, 'Style', 'pushbutton', 'String', 'Set to NaN', ...
			  'FontSize', 12, 'Units', 'norm', ...
			  'Position', [0.2, ah_p(2)+ah_p(4), 0.3, 1-ah_p(2)-ah_p(4)], ...
			  'Callback', @set_rect_to_nan);
	uicontrol('Parent', f, 'Style', 'pushbutton', 'String', 'Export', ...
			  'FontSize', 12, 'Units', 'norm', ...
			  'Position', [0.5, ah_p(2)+ah_p(4), 0.3, 1-ah_p(2)-ah_p(4)], ...
			  'Callback', @export_dat);

	function set_rect_to_nan(~,~)
		p = round(r.Position);
		rspan = p(2)+(0:p(4));
		cspan = p(1)+(0:p(3));
		% TODO: handle dims > 2
		dat(rspan, cspan) = NaN; 
		img.CData(rspan, cspan) = NaN;
		img.AlphaData(rspan, cspan) = 0;
	end
	function export_dat(~,~)
		var_name = inputdlg("Variable name to export data to:", ...
							"Export settings", [1 40], "variable_name");
		if (isempty(var_name)); return; end
		try
			assignin('base', var_name{1}, dat);
		catch e
			errordlg(sprintf('Exporting failed:\n%s', e.message), ...
					 'Export error', 'modal');
		end
	end
end
