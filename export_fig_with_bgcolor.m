function export_fig_with_bgcolor(f, clr, fn)
% exports pdf of figure with a bg-color set
% TODO:
% - help section
% - make it better
    clrs = struct('index', [], 'color', zeros(0,3));
    for i = 1:numel(f.Children)
        ch = f.Children(i);
        % for some types of children maybe we need check their children
        if isa(ch, 'matlab.graphics.axis.Axes') || ...
           isa(ch, 'matlab.graphics.illustration.Legend')
           % check if you should do it with other classes
            clrs.index(end+1) = i;
            clrs.color(end+1,:) = ch.Color;
            ch.Color = clr;
        end
    end
    exportgraphics(f, fn, 'BackgroundColor', clr, 'ContentType', 'vector');
    for i = 1:numel(clrs.index)
        set(f.Children(clrs.index(i)), 'Color', clrs.color(i,:));
    end
end

