function varargout = parse_name_value_pairs(arglist, varargin)
% quick and dirty name-value argument parser with default values.
% usage example (called inside some function with varargin):
% [name1, name2] = parse_name_value_pairs(varargin, 'name1', default_value1, ...
%													'name2', default_value2);
% [name1, name2, in_arglist] = ...
%		parse_name_value_pairs(varargin, 'name1', default_value1, ...
%										 'name2', default_value2);
% % in_arglist is a containers.Map with keys = {'name1', 'name2'} and values
% % that are logicals indicating if the name was in the arglist
%
%
% TODO
% - make it a bit more robust and such..
	do_in_arglist = false;
	if (nargout > (nargin-1)/2)
		in_arglist = containers.Map(varargin(1:2:end), ...
									false((nargin-1)/2, 1));
		do_in_arglist = true;
	end
	if (isempty(arglist))
		if (length(varargin) > 1); varargout = varargin(2:2:end); 
		else; return;
		end
	else
		varargout = cell(1, (nargin-1)/2);
		% gotta loop since two cell arrays will be strcmpi'd row-wise
		for i = 1 : length(varargout)
			j = (i-1)*2+1;
			idx = find(strcmpi(varargin{j}, arglist));
			% ensure only names and not default values are used
			if (length(idx) > 1); idx = idx(~mod(idx, 2)); end
			if (isempty(idx))
				varargout{i} = varargin{j+1};
			else
				varargout{i} = arglist{idx(1)+1};
				if (do_in_arglist); in_arglist(varargin{j}) = true; end
			end
		end
	end
	if (do_in_arglist); varargout{end+1} = in_arglist; end
end
