function varargout = fit_triangle_wave(s, varargin)
% f = fit_triangle_wave(s) returns a fit object made by fitting the signal s to
% the equation s(t) = o + a*abs(mod((t+dt)/p, 1)-0.5)*2 where the coefficients
% o (offset), a (amplitude), dt (phase_shift), and p (period) defines a
% triangular wave s(t) for t = 0:(numel(s)-1). Note that while the trailing
% factor of 2 in the equation is not strictly required it scales the amplitude
% properly.
%
% f = fit_triangle_wave(s, t) uses the provided t instead of 0:(numel(s)-1)
%
% [f, gof] = fit_triangle_wave(s, t) returns gof of fit-call
%
% [f, gof, output] = fit_triangle_wave(s, t) returns output of fit-call
%
% Why?
% The main thing this function provides are decent initial guesses - given that
% there's no, or very little baseline drift! Noise should be fine tho.
%
% Requirements
% - Curve fitting toolbox
%
% TODO
% - add a residual plot
%
% Author
% Ragnar Seton, ragnar.seton@uit.no
%
% See also
% fit, fittype
    assert(nargout < 4, 'fit_triangle_wave:nargout', ...
          'function cannot return more than 3 objects');
    if nargin > 1
        t = reshape(varargin{1},[],1);
        [tp, srt_i] = sort(t);
    else
        t = (0:(numel(s)-1))';
        tp = t;
        srt_i = true(size(t));
    end
    [sp, rmed] = rmoutliers(s(srt_i));
    tp = tp(~rmed);

    % initial guesses, offset and amplitude are pretty straight forward
    offset = min(sp);
    amplitude = max(sp) - min(sp);
    % assuming a noisy signal, the diff of the indicies of all zero crossings of
    % the centered signal will contain a bunch of small values (0Xs due to
    % noise) and a number of larger ones ("linear crossings").
    sp_mean = mean(sp);
    zx = find(diff(sp > sp_mean) ~= 0);
    zxid = diff(zx);
    n_lin_zx = sum(zxid > mean(zxid)) + 1;
    % assuming the noise causes, on average, at least one extra 0X for each
    % linear crossing n_lin_zx (number of linear zero crossings) should be
    % smaller than half of total number of them
    if n_lin_zx >= numel(zx)/2; n_lin_zx = numel(zx); end
    % TODO: test with more and less noisy signals
    period = 2*tp(end)/n_lin_zx; % period includes 2 crossings
    % phase shift such that abs(ps) < period/2..
    [~, zx0cspm_i] = maxk(abs(sp(1:zx(1))-sp_mean), ceil(zx(1)*0.05));
    if any(zx0cspm_i == 1)
        % the first value is in the top 5% of all values before zx(1), we are
        % thus unlikely to have a peak before zx(1)
        if sp(1) < sp(zx(1)); ps = -1*(tp(zx(1))-tp(1) + period/4); % rising edge
        else; ps = period/4 - (tp(zx(1))-tp(1)); % falling edge
        end
    else
        if sp(1) < sp(zx0cspm_i(1)); s = -1*tp(zx0cspm_i(1)); % rising
        else; ps = period/2 - (tp(zx0cspm_i(1))-tp(1));
        end
    end
    % make the model
    ft = fittype('o + a*abs(mod((t+dt)/p, 1)-0.5)*2', 'indep', 't', ...
                 'coef', {'o', 'a', 'dt', 'p'});
    [s_fit, gof, out] = fit(t, reshape(s,[],1), ft, ...
                            'start', [offset, amplitude, ps, period]);
    if nargout > 0
        varargout{1} = s_fit;
        if nargout > 1
            varargout{2} = gof;
            if nargout > 2; varargout{3} = out; end
        end
    else
        fprintf(['Initial guesses:\n', ...
                 '\t o =  %.4g\n', ...
                 '\t a =  %.4g\n', ...
                 '\t dt = %.4g\n', ...
                 '\t p =  %.4g\n', ...
                 'Fit results:\n'], ...
                offset, amplitude, ps, period);
        disp(s_fit);
        fprintf('GoF:\n'); disp(gof);
        fprintf('Out:\n'); disp(out);
        ah = axes('Parent', figure());
        plot(ah, t, s, '.');
        hold(ah, 'on');
        plot(ah, t, feval(s_fit, t));
        % TODO: add residual plot here
    end
end

