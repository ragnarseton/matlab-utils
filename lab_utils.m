classdef lab_utils
% utility class where i put tiny functions needed for ÅSTC lab-stuff
	properties (Constant = true)
		ARDUINO_UNO_ANALOG_PRECISION = (5-0) / 1024; % (max-min)/no_steps
	end
	methods (Static = true)
		function res = polynomial(x, p)
% Evaluates polynomial of degree length(p)-1.
% Arguments:
% x - 1D vector of variable
% p - coefficients
%
% Returns:
% res - numel(x) long vector of the evaluated polynomial
			x = reshape(x, [], 1);
			res = ones(length(x), 1) .* p(end);
			for i = 1 : length(p)-1
				res = res + x.^i .* p(end-i);
			end
		end
		function q = AWM3100V_vdc2massflow(v)
% 4th degree polyfit of VDC values given in datasheet [1].
% Argument:
% v - 1D-vector of voltage values read from the sensor
%
% Returns:
% q = massflow in sccm
%
% [1] https://sensing.honeywell.com/honeywell-sensing-airflow-awm3000-series-catalog-pages.pdf
			q = lab_utils.polynomial(v, [1.33399357591088,-14.4890483041691,61.9729464084944,-81.0296792837119,32.2252785919931]);
		end
		function dq = AWM3100V_tolvdc2massflow(dv)
% 7th degree polyfit of tolerance VDC values given in datasheet [1].
% Argument:
% dv - 1D-vector of voltage values read from the sensor
%
% Returns:
% dq - delta-massflow in sccm for given voltage
%
% [1] https://sensing.honeywell.com/honeywell-sensing-airflow-awm3000-series-catalog-pages.pdf
			dq = lab_utils.polynomial(dv, [-0.000362412967879800,0.00686993250835960,-0.0512570809942421,0.188372791031894,-0.341440622278236,0.215789853221943,0.204876051955697,-0.172848516066509]);
		end
		function mN = load_cell_mV2mN(mV)
% PROBABLY NOT USABLE as i'm not even sure where the load cell borrowed (and 
% resoldered) is anymore.
%
% Linfit of 7 measurement points taken in the lab... looked decent at the time.
% g taken from [1] with Ångström lab coords.
% Argument:
% mV - 1D-vector of millivoltage values read from the load cell
%
% Returns:
% mN - force in mN for given voltage
%
% [1] https://www.sensorsone.com/local-gravity-calculator/
			mN = lab_utils.polynomial(mV, [0.0134154674462335,3.64301203662697]).*9.81899;
		end
		function eta = dynamic_viscosity_at_temperature(gas, T)
% Polyfits of dynamic viscosities for different gases, values taken from [1] and
% [2].
% Arguments:
% gas	- gas of interest, possible values are 'air', TODO: add more
% T		- temperature [C]
%
% Returns:
% eta	- dynamic viscosity of gas at temperature T in Pa*s
%
% [1] https://www.engineeringtoolbox.com/gases-absolute-dynamic-viscosity-d_1888.html
% [2] https://www.engineeringtoolbox.com/air-absolute-kinematic-viscosity-d_601.html
			switch (lower(gas))
				case 'air'
					eta = lab_utils.polynomial(T, [6.45722817675898e-15,-2.46591608503633e-11,3.98151065842884e-08,-4.04998170727882e-05,0.0496151917125157,17.1494463877212]).*1e-6;
				otherwise
					error('Dynamic viscosity of gas ''%s'' is not yet implemented.', ...
						  gas);
			end
		end
		function q = flow_rate(dp, r, l, eta, varargin)
% Hagen–Poiseuille equation, nothing fancy just (pi*dp*r^4)/(8*eta*l).
% If units are (or are converted to) Pa, m, m, and Pa*s it will return the flow
% rate in m^3/s.
% 
% Arguments:
% dp	- Difference in pressure 
% r		- Radius of pipe
% l		- Length of pipe
% eta	- Dynamic viscosity of medium
%
% Optional args in order:
% p_unit - if it's 'mbar', 'atm', or 'Torr' p will be converted to Pa
% r_unit - if it's 'um', 'mm', 'cm', or 'in' r will be converted to m
% l_unit - as r_unit but for l
%
% TODO: make it deal with different units in a better way
%
% Example (air flow thru 125 mm long, 75 um diameter capillary with one end open
% and the other at 1.7 mbar):
%
%atm = 1013.25; p = 1.7;
%eta = lab_utils.dynamic_viscosity_at_temperature('air', 20);
%q = lab_utils.flow_rate(atm-p, 75, 125, eta, 'mbar', 'um', 'mm');
%air_kg_p_m3 = 1.2;
%air_kg_p_mol = 0.029;
%flow_rate_in_mol_p_s = q * air_kg_p_m3 / air_kg_p_mol;
			if (nargin > 4)
				switch (lower(varargin{1}))
					case 'mbar'
						dp = dp * 100;
					case 'atm'
						dp = dp * 101325;
					case 'torr'
						dp = dp * 133.322;
					otherwise
						error('pressure unit ''%s'' is not yet implemented.', ...
							  varargin{1});
				end
			end
			if (nargin > 5); r = len2m(r, lower(varargin{2}));
			end
			if (nargin > 6); l = len2m(l, lower(varargin{3}));
			end
			q = (pi*dp*r^4)/(8*eta*l);

            function l = len2m(l, unit)
				switch (unit)
					case 'um'
						l = l / 1e6;
					case 'mm'
						l = l / 1e3;
					case 'cm'
						l = l / 1e2;
					case 'in'
						l = l * 0.0254;
					otherwise
						error('length unit ''%s'' is not yet implemented.', ...
							  unit);
				end
            end
		end
		function [R_hyd, varargout] = flow_resistance(geometry, perimeter, area, eta, chan_length)
% Calculates the flow resistance in the way described, and with the assumptions
% made, in [1].
% Arguments:
% geometry    - geometry of the channel, possible values are:
%			    'iso_triangle', TODO: add more gemoetries
% perimeter   - length of perimeter in meters
% area		  - area of cross section in m^2
% eta		  - dynamic viscosity of gas flowing in the channel
% chan_length - length of channel in meters
%
% Returns:
% R_hyd		  - Hydrodynamic resistance in Pa*s/m^3
%
% Optional Returns (in order):
% alpha		  - Scaling factor with the same name in [1]
% R_p		  - R*_hyd in [1]
%
% Here's the jist of the theory from the paper:
% q: flow, since we're assuming straight channel with a steady state flow, q is
%    given by integrating the velocity, v(x,y), over the cross section A (here
%    with no-slip condition at walls)
% dp: pressure diff over channel
% eta: dynamic viscosity
% L: channel length
% A: cross sectional area
% P: perimeter of cross section
%
% R_hyd = dp / q  =>  dp = R_hyd*q
% R_p = eta * L / A^2
%
% definitions (both unitless):
% alpha = R_hyd / R_p
% C = P^2/A
%
% leads to:
% dp = R_hyd*q = alpha * R_p * q
%
% and then there are different ways of calcing alpha from C..
%
% [1] DOI: 10.1103/PhysRevE.71.057301

			compactness = (perimeter.^2) ./ area;

			% translations invariance in e_z gets rid of some nonlinear term in
			% Navier-Stokes, that we needs to looks intos! can we safely approx the
			% spirals as straight?
			
			switch (lower(geometry))
				case 'iso_triangle'
					alpha = (25/17) .* compactness + (sqrt(3)*40/17);
				otherwise
					error('Geometry ''%s'' is not yet implemented.', geometry);
			end
			R_p = eta .* chan_length ./ area.^2;
			R_hyd = alpha .* R_p;
			
%			R_hyd = chan_length / (200e-6)^4;
			
			if (nargout > 1); varargout{1} = alpha;
			if (nargout > 2); varargout{2} = R_p; end; end
		end

		function nu = PDMS_viscosity(T, nu25)
% Implementation of equation 13 from https://doi.org/10.3390/ma14206060
% By default it returns the DYNAMIC viscosity [Pa*s] of PDMS given the
% temperature T and KINEMATIC viscosity of it at 25 °C nu25 (F(T,nu) -> mu is
% what the paper modelled so that's what you get here). Since you never remember
% it: nu = mu / rho, where nu is the kinematic viscosity [m^2/s], mu the dynamic
% one [Pa*s = N/m^2 * s = kg/(m*s)], and rho the density [kg/m^3].
%
% Uncured but catalyzed dynamic viscosity and density of PDMSs in the chemilab:
% Sylgard 184:        3.5 Pa*s, 1030 kg/m^3 
% Elastosil Vario 15: 3 Pa*s,   1060 kg/m^3
% True Skin 10:       5 Pa*s,   1080 kg/m^3
%
% ARGS:
% T    - temperature(s)
% nu25 - kinematic viscosity of PDMS at 25 °C
			arguments
				T (1,:) {mustBeNumeric}
				nu25 (1,1) {mustBeNumeric}
			end
			L = @(T_i) (T_i + 21.64) ./ 44.92;
			nu = nu25 .* (2561.34.*L(T).^(-0.08) .* exp(-L(T).^0.92) + 100.56);
		end

		function [min_v, varargout] = stable_min(q, sg_fln, no_lf_pts, step_sz)
% Easy (but can get very slow depending on the arguments) way to get a somewhat
% stable (approximate) min-value from a noisy signal.
% It's done by savgol smoothing the signal with sg_order/sg_fln (sg_order will
% be an arg when i get to around to it, sg_fln being the frame len), then
% linfitting segments of no_lf_pts length separated by step_sz and finding the
% linfit where beta < b_threshold and r_square < rs_threshold that has the
% lowest mean value.
%
% Arguments:
% q			- signal to find stable min in
% sg_fln	- savgol filter framelen
% no_lf_pts - number of points to use for linear fit
% step_sz	- number of points to step between each linear fit
%
% Returns:
% min_v		- approximate stable min value of signal
%
% Optional returns (in order):
% min_i		- approximate index of stable min in q
% sq		- smoothed signal
% lf_min	- the linear fit that approximate min was taken from
%
% TODO:
% - make b_threshold, rs_threshold, sg_order, sg_fln and sg_fln optional args
% - rewrite the whole thing such that it (still based on smoothed signal):
%	1) start at min
%	2) check if it's good wrt thresholds
%		a) if it is, return
%		b) if not, set point to NaN and return to (1)
			b_th = 0.05;
			rs_th = 0.3;
			sg_order = 3;

			X_i = (1:no_lf_pts)';
			X = [ones(no_lf_pts, 1), X_i];
			% start by smoothing the signal with savgolay filter 
			sq = sgolayfilt(q, sg_order, sg_fln);
			% find min by linfitting, thresholding beta and then averaging
			min_v = inf;
			for j = 0 : step_sz : length(sq)-no_lf_pts
				y = reshape(sq(j + X_i), [], 1);
				y_n = y./max(y);
				b = X \ y_n;
				if (abs(b(2)) < b_th)
					if (sum((y_n-X*b).^2)/sum((y_n-mean(y_n)).^2) < rs_th && ...
						mean(y) < min_v)
						min_v = mean(y);
						if (nargout > 1)
							varargout{1} = j;
							if (nargout > 3)
								min_b = b;
								min_y = max(y);
							end
						end
					end
				end
			end
			if (nargout > 1)
				if (isinf(min_v))
					varargout{1} = 0;
					if (nargout > 2); varargout{2} = 0;
					if (nargout > 3); varargout{3} = 0;
					end
					end
					return;
				end
				j = varargout{1};
				varargout{1} = floor(varargout{1} + 1 + no_lf_pts/2);
				if (nargout > 2)
					varargout{2} = sq;
					if (nargout > 3)
						varargout{3} = [ j + X_i, ...
										 reshape(X*(min_b*min_y), [], 1) ];
					end
				end
			end
        end

        function flats = flatfinder(sig, pre_p_wf, post_p_wf, min_sw_factor)
% finds flat sections of a step signal and returns a Nx2 matrix of (start and
% stop) indicies.
%
% args:
% sig           - signal to find the flat parts of
% pre_p_wf      - factor to multiply diffed peak FWHM (see algo below) with to
%                 get the length before the corresponding vertical section in
%                 the signal to skip. Recommended value: 2
% post_p_wf     - as pre_p_wf but to skip after vertical section in sig.
%                 Recommended value: 4.5
% min_sw_factor - factor to multiply initially found number of steps with to
%                 get the minimum step width. Recommended value: 0.5;
%
% Algo (essentially dS/dt < small_value):
% 1) find peaks in abs(diff(movmean(sig)))
% 2) remove peaks that are too close to each other
% 3) take the width of each peak (times pre/post_p_wf) and remove that around
%    the peaks in sig
% 4) what's left are your flat steps
%
% Note:
% if your signal contains many (>1e5) points you should have a look at the
% arbitrary numbers in here.
            sig = reshape(sig, 1, []);
            % WARNING!! arbitrary numbers ahead!
            ds = abs(diff(movmean(sig, max(round(length(sig)/500), 10))));
            [p_h, p_i, p_w, ~] = findpeaks(ds, "MinPeakHeight", std(ds)*2);
            if length(p_h) < 1
                flats = [];
                return;
            end
            % remove any smaller peaks that are very close to a larger one
            dpi = diff(p_i);
            to_rm = find(2.*dpi < mean(dpi))+1;
            for i = sort(to_rm, 'descend')
                if p_h(i) > p_h(i-1)
                    p_i(i-1) = [];
                    p_w(i-1) = [];
                else
                    p_i(i) = [];
                    p_w(i) = [];
                end
            end
        
            min_step_w = min_sw_factor * length(sig)/(length(p_i)+1);
            % allocate enough to have flats outside of both first and last peak
            flats = zeros(length(p_i)+1, 2);
            f_c = 0;
            % then we first check before the first peak
            if p_i(1) >= min_step_w
                f_c = f_c + 1;
                flats(1,:) = [1, round(p_i(1) - pre_p_wf*p_w(1))];
            end
            % store all the ones between the peaks
            for i = 2:length(p_i)
                f_c = f_c + 1;
                flats(f_c, :) = [round(p_i(i-1) + post_p_wf*p_w(i-1)), ...
                                 round(p_i(i) - pre_p_wf*p_w(i))];
            end
            % and finally after the last peak
            if (length(sig) - p_i(end)) >= min_step_w
                f_c = f_c + 1;
                flats(f_c,:) = [round(p_i(end) + post_p_wf*p_w(end)), ...
                                length(sig)];
            end
            % remove empty ones in case we don't have flats before and after
            if f_c < size(flats, 1); flats = flats(1:f_c, :); end
        end
    end
end
