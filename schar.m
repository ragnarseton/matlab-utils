function sc = schar(c)
% returns char(charcode) of special character c, if c is empty all available
% chars are returned in a map with their char codes
	m = containers.Map({'deg', 'degree', 'pm', 'plusminus', 'pwr2', 'pwr3', ...
						'mu', 'micro', 'dot', 'pwr1', '1/4', '1/2', '3/4'}, ...
						[176, 176, 177, 177, 178, 179, ...
						 181, 181, 183, 185, 188, 189, 190]);
	if (~isempty(c)); sc = char(m(c));
	else; sc = m;
	end
end
