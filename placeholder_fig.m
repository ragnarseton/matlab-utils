function varargout = placeholder_fig(xlbl, ylbl, varargin)
% args: xlabel, ylabel
%
% available name-value pairs:
% 'AxesArgs'		- cell array with arguments to the axes call
% 'LegendArgs'		- cell array with arguments to the legend call
% 'Legends'			- cell array with legends, numel of the cell array lines
%					  will be plotted in the axes (this has priority over the
%					  'NoLines' argument)
% 'LineStyle'		- either 'line', which sets 'PlaceholderFn' to
%					  @(~) sin(linspace(0,1,100).*2*pi).*rand() and 'XData' to
%					  linspace(0,1,100), or 'point' which sets it to
%					  @(i) i*rand() and i, given that 'PlaceholderFn' and
%					  'XData' are not given, default: 'line'
% 'NoLines'			- integer giving the number of placeholder lines (unless
%					  'Legends' is given with different number of elements),
%					  default: 1
% 'NormY'			- logical, if true the output of PlaceholderFn will be
%					  mapped to [0 1], scaled by 1/(NoLines+1), and translated
%					  by (i-1)/(NoLines+1), if false output will not be altered
%					  at all,default: true
% 'PlaceholderText'	- char array or string with placeholder text, default:
%					  'placeholder'
% 'PlaceholderFn'	- function handle to generate placeholder line data (output
%					  will be scaled down) from input i - the line number,
%					  default is given by 'Style' argument
% 'PlotArgs'		- cell array with arguments to the plot call(s) when
%					  plotting the placeholder lines
% 'TextArgs'		- cell array with arguments to the text call, default:
%					  {'HorizontalAlignment', 'center'}
% 'Title'			- char array (or string if you're into that kinda thing)
%					  with the title
% 'XData'			- vector of x-values used when plotting, default value
%					  depends on 'LineStyle'
%
% returns:
% f = placeholder_fig(xlbl, ylbl); % returns figure handle
% [f, a] = placeholder_fig(xlbl, ylbl); % axes handle
% [f, a, t] = placeholder_fig(xlbl, ylbl); % text handle
%
% dependencies:
% - pare_name_value_pairs.m
	[aargs, lgnds, lgndargs, linestyle, no_lines, normy, tit, txt, txtargs, line_fn, pltargs, x, in_args] = ...
		parse_name_value_pairs(varargin, 'AxesArgs', cell(0), ...
										 'Legends', cell(0), ...
										 'LegendArgs', cell(0), ...
										 'LineStyle', 'line', ...
										 'NoLines', -1, ...
										 'NormY', true, ...
										 'Title', '', ...
										 'PlaceholderText', 'placeholder', ...
										 'TextArgs', {'HorizontalAlignment', ...
													  'center'}, ...
										 'PlaceholderFn', @(~) sin(linspace(0,1,100).*2*pi).*rand(), ...
										 'PlotArgs', cell(0), ...
										 'XData', linspace(0, 1, 100));
	use_i_as_x = false;
	marker = '-';
	if (strcmpi(linestyle, 'point'))
		marker = 'o';
		if (~in_args('PlaceholderFn')); line_fn = @(i) i*rand(); end
		if (~in_args('XData'))
			use_i_as_x = true;
			x = 0.5; % in case no_lines ends up being 1
		end
	end
	sinca = @(ca, s) any(cellfun(@(c) ((ischar(c) || isstring(c)) && strcmpi(c, s)), ca));
	f = figure();
	ah = axes('Parent', f, aargs{:});
	norm01 = @(y) (y - (length(y) > 1)*min(y)) ./ (max(y) - min(y) + y*(length(y) < 2));
	got_lgnds = numel(lgnds) > 0;
	if (got_lgnds); no_lines = numel(lgnds); end
	if (no_lines < 0)
		plot(ah, x, norm01(line_fn(1)), marker, pltargs{:});
	elseif (no_lines > 0) % if no_lines == 0 caller wants an empty placeholder
		hold(ah, 'on');
		for i = 1 : no_lines
			if (use_i_as_x); x = i/(no_lines+1); end
			if (normy); y = (i-1)/(no_lines+1) + norm01(line_fn(i))./(no_lines+1);
			else; y = line_fn(i);
			end
			ph = plot(ah, x, y, marker, pltargs{:});
			if (got_lgnds); set(ph, 'DisplayName', lgnds{i}); end
		end
		if (numel(lgnds) > 0); legend(ah, lgndargs{:}); end
	end
	xlabel(ah, xlbl);
	ylabel(ah, ylbl);
	if (~isempty(tit)); title(ah, tit); end
	th = text(ah, 0.5, 0.5, txt, txtargs{:});
	% not linkprop()ing the colors, since we don't want 'none' for th
	if (~sinca(txtargs(1:2:end), 'BackgroundColor'))
		clr = ah.Color;
		% setting ah.Color = 'r' or '#ff0000' will still return [1 0 0], only
		% setting it to 'none' returns a char (as far as i've seen it)
		if (ischar(clr))
			clr = f.Color;
			if (ischar(clr)); clr = [1 1 1]; end
		end
		th.BackgroundColor = clr;
	end
	if (~sinca(txtargs(1:2:end), 'EdgeColor'))
		clr = th.BackgroundColor;
		if (ischar(clr)); clr = [0 0 0];
		else; clr = 1 - clr;
		end
		th.EdgeColor = clr;
	end
	if (normy && ~in_args('XData'))
		xlim(ah, [0 1]);
		ylim(ah, [0 1]);
	end
	if (nargout > 0); varargout{1} = f;
		if (nargout > 1); varargout{2} = ah; 
			if (nargout > 2); varargout{3} = th; end
		end
	end
end
