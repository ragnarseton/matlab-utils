function [x, y, header] = readThorlabsCSV(fn)
% [x, y, header] = readThorlabsCSV(fileName)
%	Reads a CSV produced by Thorlabs' OSA software. fileName shuold be a valid
%	absolute or relative path to a file which the running instance of matlab
%	has read permissions.
%	The header contains all the fields in the [SpectrumHeader]-section,
%	example:
% header = 
%   struct with fields:
% 
%                     Date: 20170831
%                     Time: 14070110
%                  GMTTime: 12070110
%                XAxisUnit: 'nm_air'
%                YAxisUnit: 'intensity'
%                  Average: 1000
%           RollingAverage: 0
%           SpectrumSmooth: 0
%            SSmoothParam1: 0
%            SSmoothParam2: 0
%            SSmoothParam3: 0
%            SSmoothParam4: 0
%          IntegrationTime: 150
%              TriggerMode: 0
%     InterferometerSerial: 'M00453788'
%                   Source: ''
%            AirMeasureOpt: 0
%                   WnrMin: 1.3465e+04
%                   WnrMax: 3.1092e+04
%                   Length: 3648
%               Resolution: 0
%                      ADC: 0
%               Instrument: 0
%               InstrModel: 'CCS100'
%                     Type: 'emission'
%                  AirTemp: 0
%              AirPressure: 0
%                AirRelHum: 0
%                     Name: ''
%                  Comment: [1×0 char]
	BASE_ID = 'readThorlabsCSV';
	assert(isa(fn, 'char') || isa(fn, 'string'), ...
		   [BASE_ID, ':argClass'], ...
		   'Argument must be of class char but is %s.', class(fn));
	[fid, errmsg] = fopen(fn, 'rt');
	assert(fid ~= -1, ...
		   [BASE_ID, ':fopen'], ...
		   'Failed to open file ''%s'': %s', fn, errmsg);
	ln = lower(fgetl(fid));
	if (~contains(ln, 'thorlabs'))
		fclose(fid);
		error([BASE_ID, ':identLine'], ...
			  'First line does not contain ''thorlabs''.');
	end

	% find and fetch header
	while (~feof(fid) && ~contains(ln, '[SpectrumHeader]'))
		ln = fgetl(fid);
	end
	if (feof(fid))
		warning('''%s'' does not contain any header info.', fn);
		fclose(fid);
		return;
	end
	header = struct();
	ln = fgetl(fid);
	while (~feof(fid) && ~isempty(ln) && ln(1) == '#')
		keyValue = strsplit(ln(2:end), '\t', ...
							'CollapseDelimiters', true);
		if (length(keyValue) == 2)
			if (regexp(keyValue{2}, '^[\d\.eEdD\+-]+$'))
				v = str2double(keyValue{2});
			else
				v = keyValue{2};
			end
		elseif (length(keyValue) < 2)
			warning([BASE_ID, ':headerSection:keyValue:lt2'], ...
					'Malformatted header field:\n''%s''', ln);
			ln = fgetl(fid);
			continue;
		elseif (length(keyValue) > 2)
			v = keyValue{2};
			if (v(1) ~= '"')
			warning([BASE_ID, ':headerSection:keyValue:gt2'], ...
					'Malformatted header field:\n''%s''', ln);
				ln = fgetl(fid);
				continue;
			end
			v = strjoin('\t');
		end
		if (isa(v, 'char') && length(v) > 1 && v(1) == '"' && v(end) == '"')
			v = v(2:end-1);
		end
		if (regexp(keyValue{1}, '^\w+$'))
			k = keyValue{1};
		else
			warning([BASE_ID, ':headerSection:key'], ...
					'Malformatted header field:\n''%s''', ln);
			ln = fgetl(fid);
			continue;
		end
		header.(k) = v;
		ln = fgetl(fid);
	end
	if (feof(fid))
		warning([BASE_ID, ':dataSection:EOF'], ...
				'''%s'' does not contain any data.', fn);
		fclose(fid);
		return;
	end
	
	% read the data
	while (~feof(fid) && ~contains(ln, '[Data]'))
		ln = fgetl(fid);
	end
	if (feof(fid))
		warning([BASE_ID, ':dataSection:headerThenEOF'], ...
				'''%s'' does not contain any data (but does contain [Data] section).', fn);
		fclose(fid);
		return;
	end
	if (isfield(header, 'Length') && isa(header.Length, 'double'))
		% if we have a length we just trust and run with it!
		vals = textscan(fid, '%f\t%f', header.Length);
		x = vals{1};
		y = vals{2};
	else
		% if we don't, there's a whole host of stuff that needs to be done
		warning([BASE_ID, ':headerSection:lengthMissing'], ...
				'Header section missing Length-field, parsing manually.');
		fo = ftell(fid);
		if (fseek(fid, 0, 'eof') ~= 0)
			fclose(fid);
			error([BASE_ID, ':dataSection:fseek:EOF'], ...
				  'fseek end failed, cannot fetch data.');
		end
		fe = ftell(fid) - 12; % account for \n[EndOfFile]
		if (fseek(fid, fo, 'bof') ~= 0)
			fclose(fid);
			error([BASE_ID, ':dataSection:fseek:BOF'], ...
				  'fseek back failed, cannot fetch data.');
		end
		% guessing length, assuming ASCII lines formatted as
		% -7.426541138e+002\t-3.154909471e-003\n
		noRows = floor((fe - fo) / 36);
		x = zeros(noRows, 1);
		y = zeros(noRows, 1);
		i = 1;
		ln = fgetl(fid);
		while (~feof(fid) && ...
			   ((48 <= double(ln(1)) && double(ln(1)) <= 57) || ln(1) == '-' || ln(1) == '+'))
			vals = textscan(ln, '%f\t%f');
			x(i) = vals{1};
			y(i) = vals{2};
			ln = fgetl(fid);
			i = i + 1;
		end
		if i < 3
		warning([BASE_ID, ':dataSection:headerThenEOFwoLength'], ...
				'''%s'' does not contain any data (but does contain [Data] section).', fn);
			fclose(fid);
			x = [];
			y = [];
			return;
		elseif i-1 < noRows
			x = x(1:i-1);
			y = y(1:i-1);
		end
	end
	% ignore any further content (should just be '[EndOfFile]')
	fclose(fid);
end

