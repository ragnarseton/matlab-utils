function dump_axes_view(varargin)
% dumps out code to get the current view of 3D-plot (so you can easily recreate
% it). the whole function is pretty shit. i was in hurry but needed to do it for
% a bunch of 3D plots.
%
% optional args:
% ah    - axes handle [gca()]
    if nargin > 0; ah = varargin{1};
    else; ah = gca();
    end
    assert(isa(ah, 'matlab.graphics.axis.Axes'), ...
           'gotta have an axes handle, buddy boi');
    props = {'View', 'Projection', 'CameraPosition', 'CameraTarget', ...
             'CameraUpVector', 'CameraViewAngle'};
    for i = 1:numel(props)
        fprintf('ah.%s = ', props{i});
        prettyprintln(ah.(props{i}));
    end
    function prettyprintln(v)
        if isnumeric(v)
            if numel(v) == 1; fprintf('%g;\n', v);
            else; fprintf(['[', strjoin(sprintfc('%g', v), ', '), '];\n']);
            end
        elseif ischar(v) || isstring(v)
            fprintf('''%s'';\n', v);
        end
        % for more classes see
        % https://gitlab.com/ragnarseton/view_zygo/-/blob/master/view_zygo.m#L878
    end
end

